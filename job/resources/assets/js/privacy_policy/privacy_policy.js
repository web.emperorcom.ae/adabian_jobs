$(document).ready(function () {
    $('#description').summernote({
        minHeight: 200,
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['para', ['paragraph']],
        ],
    });

    $('#privacyPolicy').submit(function () {
        if ($('#description').summernote('isEmpty')) {
            displayErrorMessage('Privacy Policy field is required');
            return false;
        }
    });

    $('#termsConditions').submit(function () {
        if ($('#description').summernote('isEmpty')) {
            displayErrorMessage('Terms Conditions field is required');
            return false;
        }
    });
});
