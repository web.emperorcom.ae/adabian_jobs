
    <div class="col-10 job-post-main">
    
       







    <div class="container">
					<div class="row">
                  
    <div class=" col-lg-8">
    <ul class="post-job-bx browse-job-grid row">
    @forelse($jobs as $job)
    
						<li >
							<div class="post-bx">
               
								<div class="d-flex m-b30">
									<div class="job-post-info">
										<h5><a href="{{ route('front.job.details',$job['job_id']) }}">{{ $job['job_title'] }} </a></h5>
                                        <br> <h6> {{ $job->company->user->first_name }}</h6>
										<ul>
											<li><i class="fa fa-map-marker"></i> {{ (!empty($job->full_location)) ? $job->full_location : 'Location Info. not available.'}}</li>
											<li><i class="fa fa-bookmark-o"></i> Full Time</li>
											
										</ul>
									</div>
								</div>
								<div class="d-flex">
									<div class="job-time mr-auto">
                                    <h7>      {!! nl2br(Str::limit($job['description'],120)) !!}  </h7>
									</div>
									
								</div>
								<label class="like-btn">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>

						</li>
						

                       
                        @empty
            <div class="col-md-12 text-center">{{ __('web.job_menu.no_results_found') }}</div>
        @endforelse					
						
        @if($jobs->count() > 0)
            {{$jobs->links() }}
        @endif		
						
					
						
					</ul>
                    </div>
                    <div class="col-lg-4">
										<div class="widget bg-white p-lr20 p-t20  widget_getintuch radius-sm">
											<h7 class="text-black ">Get new jobs  by email</h7>
											
                                            <hr>


                                            <div class="input-group">
										<input type="text" class="form-control" placeholder="Job Title, Keywords, or Phrase">
										
									</div>
                                    <br>
                                            <a href="#" title="READ MORE" rel="bookmark" data-toggle="modal" data-target="#car-details" class="site-button"> Activate </a>
                                            <br>
                                   <p style="
    font-size: 11.5px;
">By creating a job alert, you agree to our Terms. You can change your consent settings at any time by unsubscribing or as detailed in our terms.</p>
                                    
                                    </div>
                                
                                    <br> <br>
                        </div>
                        </div>


       
        
        </div>
