<div class="table-responsive-sm">
    <table class="table table-striped table-bordered" id="candidatesTbl">
        <thead>
        <tr>
            <th scope="col">{{ __('messages.common.name') }}</th>
            <th scope="col">{{ __('messages.candidate.email') }}</th>
            <th scope="col">{{ __('messages.candidate.industry') }}</th>
            <th scope="col">{{ __('messages.candidate.immediate_available') }}</th>
            <th scope="col">{{ __('messages.common.email') }} {{ __('messages.common.verified') }}</th>
            <th scope="col">{{ __('messages.common.resend_verification_mail') }}</th>
            <th scope="col">{{ __('messages.common.status') }}</th>
            <th scope="col">{{ __('messages.common.action') }}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
