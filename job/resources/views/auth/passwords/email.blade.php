@extends('web.layouts.apps')
@section('title')
    {{ __('web.login') }}
@endsection
@section('content')
   
<div class="page-content bg-white login-style2"  style="background-image:url(../images/background/bg6.jpg); background-size: cover;">
        <div class="section-full">
            <!-- Login Page -->
            <div class="container">
                <div class="row">
					<div class="col-lg-4 d-flex">
						<div class="text-white max-w400 align-self-center">
						<div class="logo">
                            <a href="#"><img src="http://jobsite.emperordigital.us/images/logo-white.png" class="logo" alt=""></a>


							</div>
							<h2 class="m-b10">Login To You Now</h2>
							<p class="m-b30">If you have Account Login Here!!</p>
							<ul class="list-inline m-a0">
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					
                    <div class="col-lg-8">
                       
                       
                            <div class="login-2 submit-resume p-a30 seth">
                                <div class="tab-content nav">
                                    <div class="tab-content">

                                    @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                <!-- Start of Nav Tabs -->
                
                
                <form method="POST" action="{{ route('password.email') }}">
                                                @csrf
                                <div id="candidateValidationErrBox">
                                    @include('layouts.errors')
                                </div>
                                <input type="hidden" name="type" value="1"/>
                                <div class="form-group">
                                <label for="email">Email</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" tabindex="1" value="{{ old('email') }}" autofocus required placeholder="Registered Email"> 
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>

                    



                                </div>
                                <br>
                                
                               
                                <div class="form-group text-center ml20">
                                    <div class="social-login-buttons d-flex flex-md-wrap justify-content-center">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:309px;">Send Reset Link</button>
                                    </div>
                                </div>
                                                </form>







<div class="mt-5 text-muted text-center">
        Recalled your login info? <a href="{{ route('login') }}">Sign In</a>
    </div>
               
                                         

                                        





                                    </div>

                                </div>
                             </div>
                           



                    </div>



                    
				</div>
			</div>
			<!-- Login Page END -->
		</div>
		<footer class="login-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<span class="float-left">© Copyright 2020  by 
						<a href="http://emperordigital.ae/" target="_blank">Emperor Digital </a> </span>
						<span class="float-right">
							All rights reserved.
						</span>
					</div>
				</div>
			</div>
		</footer>
	</div>




    
@endsection