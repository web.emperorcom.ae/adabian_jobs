@extends('web.layouts.app')
@section('title')
{{ __('web.job_details.apply_for_job') }}
@endsection
@section('css')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

<div class="container">
       <div class="row">
        <div class="col-lg-5">
          <div class="search-heading">
            <h5>What</h5>
          </div>
          <form  action="{{ route('front.search.jobs') }}"
                          method="get">
          <div class="search-title">
              <p>Job title, keywords, or company </p>
            </div>
          <div class="search-field">
            <input type="text" class="form-control" placeholder="Job Title,keywords, or company">
          </div>

        </div>
      
        <div class="col-lg-5">
          <div class="search-heading">
            <h5>Where</h5>
           </div>
           <div class="search-title">
            <p>City, Emirate, Place or Location </p>
            </div>
          <div class="search-field">
            <input type="text" class="form-control" placeholder="City, Emirate, Place or Location">
          </div>
          </div>
        
        <div class="col-lg-2">
         <div class="job-btn">
            <button type="submit" class="btn btn-primary">Find Jobs</button>
          </div>
        </div>

</form>
      </div>
    </div>

<div class="container">
    <div class="row">
        
        <div class="col-lg-8">
   
           <div class="card mb-4 box-shadow">
                <div class="card-body">
                   <h2>{{ __('web.apply_for_job.apply_for') }} <span class="text-blue">{{ $job->job_title }}</span></h2>
                   @if($isApplied)
                <h3 class="uppercase text-blue">{{ __('web.job_details.already_applied') }}</h3>
                <div class="row account-question">
                    <div class="col-md-10 nopadding">
                        <p class="nomargin">
                            {{ __('web.apply_for_job.we_received_your_application') }}
                        </p>
                    </div>
                </div>
            @else
            <form id="applyJobForm" class="post-job-resume mt50">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="{{ isset($job) ? $job->id : null }}" name="job_id">
                            <div class="form-group col-sm-12 mt10 ">
                                <label for="resumeId">{{ __('messages.apply_job.resume').':' }}<span
                                            class="required asterisk-size">*</span></label>
                                {{ Form::select('resume_id', $resumes, ($isJobDrafted) ? $draftJobDetails->resume_id : null, ['class' => 'btn btn-primary','id' => 'resumeId','placeholder'=>'Select Resume', 'required']) }}
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="expected_salary">{{ __('messages.candidate.expected_salary').':' }}<span
                                            class="required asterisk-size">*</span></label>
                                <input type="text" id="expected_salary" name="expected_salary"
                                       value="{{ ($isJobDrafted) ? $draftJobDetails->expected_salary : '' }}"
                                       class="form-control price-input" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="notes">{{ __('messages.apply_job.notes').':' }}</label>
                                <textarea rows="5" id="notes" name="notes"
                                          class="form-control">{{ ($isJobDrafted) ? $draftJobDetails->notes : '' }}</textarea>
                            </div>
                            <div class="form-group pt30 nomargin text-right" id="last">
                                @if(!$isApplied)
                                    @if(!$isJobDrafted)
                                        <button class="btn btn-primary save-draft mr-1">Save as Draft</button>
                                    @endif
                                    @if($isActive && !$job->is_suspended)
                                        <button class="btn btn-primary apply-job">{{ __('web.common.apply') }}</button>
                                    @endif
                                @else
                                    <button class="btn btn-sucess btn-effect">{{ __('web.apply_for_job.already_applied') }}</button>
                                @endif

                            </div>
                        </div>
                    </div>

                </form>
            @endif

                </div>
            </div>
        </div>

        <div class="col-lg-4">
   
        <div class="card mb-4 box-shadow">
            <div class="card-body">
            <div class="job-sidebar">
            <h5>{{ __('web.job_details.company_overview') }}</h5>
            <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a href="#">
                                        <img src="{{ $job->company->company_url }}"
                                             class="c-company-image company-image"/>
                                    </a>

                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 mt10">
                                    <a href="#">
                                        <h5 class="text-primary">{{ $job->company->user->first_name }}</h5>
                                    </a>
                                    <div class="text-dark c-company-p pt10 pb10">
                                        <i class="fa fa-map-marker"></i>
                                        <span>
                                            @if (!empty($job->company->city_name))
                                                {{$job->company->city_name}} ,
                                            @endif

                                            @if (!empty($job->company->state_name))
                                                {{$job->company->state_name}},
                                            @endif

                                            @if (!empty($job->company->country_name))
                                                {{$job->company->country_name}}
                                            @endif

                                            @if(empty($job->company->country_name))
                                                {{ __('web.job_details.location_information_not_available') }}
                                            @endif
                                        </span>
                                    </div>
                                    <h6>
                                  
                                       
                                    </h6>
                                    <hr/>
                                    <p>
                                        {!! nl2br($job->company->details) !!}
                                    </p>
                                </div>
                              

             </div>
            
            </div>
        </div>

    </div> 
    </div>
</div>

   
    <!-- ===== Start of Main Wrapper Job Section ===== -->
    


@endsection
@section('scripts')
    <script>
        let applyJobUrl = "{{ route('apply-job') }}";
        let jobDetailsUrl = "{{ url('job-details') }}";
        $('#resumeId').selectpicker();
    </script>
    <script src="{{mix('assets/js/custom/input_price_format.js')}}"></script>
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="{{ mix('assets/js/jobs/front/apply_job.js') }}"></script>
@endsection

