@extends('web.layouts.app')
@section('title')
    {{ __('web.job_details.job_details') }}
@endsection
@section('content')
<div class="container mTop">
            @if($job->is_suspended || !$isActive)
                <div class="row">
                    <div class="alert alert-warning text-warning bg-transparent" role="alert">
                        {{ __('web.job_details.job_is') }} <strong> {{\App\Models\Job::STATUS[$job->status]}}.</strong>
                    </div>
                </div>
            @endif
            @if(Session::has('warning'))
                <div class="alert alert-warning" role="alert">
                    {{ Session::get('warning') }}
                    <a href="{{ route('candidate.profile',['section'=> 'resumes']) }}"
                       class="alert-link ml-2 ">{{ __('web.job_details.click_here') }}</a> {{ __('web.job_details.to_upload_resume') }}
                    .
                </div>
            @endif
</div>

<div class="page-content">
        <!-- inner page banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white"> Job Details</h1>
					<!-- Breadcrumb row -->
					<div class="breadcrumb-row">
						<ul class="list-inline">
							<li><a href="#">Home</a></li>
							<li> Job Details</li>
						</ul>
					</div>
					<!-- Breadcrumb row END -->
                </div>
            </div>
            <div class="section-full browse-job-find">
			<div class="container">
				<div class="find-job-bx">
					
					<form  class="dezPlaceAni" action="{{ route('front.search.jobs') }}" method="get">
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group">
									
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Job Title, Keywords, or Phrase">
										<div class="input-group-append">
										  <span class="input-group-text"><i class="fa fa-search"></i></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									
									<div class="input-group">
										<input type="text" class="form-control" placeholder="City, State or ZIP">
										<div class="input-group-append">
										  <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-lg-2">
								<button type="submit" class="site-button btn-block">Find Job</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
        </div>
    <br><br><br>
<div class="content-block">
			<!-- Browse Jobs -->
			<div class="section-full browse-job content-inner-2">
				<div class="container">
					<div class="job-bx bg-white">
						<div class="row">
							<div class="col-lg-8">
                            <div class="compy-logo">
            <a href="#">
              <img src="{{ $job->company->company_url }}" alt="">
            </a>
        </div>
  
    <div class="job-Titleroles">
    <h6 class="capitalize">{{ Str::limit($job->job_title,50,'...') }}</h6>
    <p class="capitalize">{{ $job->company->user->first_name }}-{{ $job->company->state_name }} </p>
    <p class="capitalize"> {{ ($job->jobType) ? $job->jobType->name : __('messages.common.n/a') }}</p>
    
                                @auth
                                @role('Candidate')
                                @if(!$isApplied && !$isJobApplicationRejected && ! $isJobApplicationCompleted)
                                @if($isActive && !$job->is_suspended && \Carbon\Carbon::today()->toDateString() < $job->job_expiry_date->toDateString())
                                <button
                                            class="btn {{ $isJobDrafted ? 'btn-red' : 'btn-purple' }} btn btn-outline-primary"
                                            onclick="window.location='{{ route('show.apply-job-form', $job->job_id) }}'">
                                            {{ $isJobDrafted ? __('web.job_details.edit_draft') : __('web.job_details.apply_for_job') }}
                                        </button>
                                    @endif

                                  
                              
                                       <button  class="btn btn-outline-primary"  data-favorite-user-id="{{ (getLoggedInUserId() !== null) ? getLoggedInUserId() : null }}"
                                                        data-favorite-job-id="{{ $job->id }}" id="addToFavourite"><span
                                                    class="label label-info mt20">Add To Favorite</span></button>
                                                

                                                    @else
                               
                               <p>
                                   <button class="btn btn-outline-primary">{{ __('web.job_details.already_applied') }}</button>
                               </p>

                                                    @endif
                                @endrole
                                @endauth   
                                
                                <hr>

        </div>
        <div class="job-Details">
            <p>Job Description</p>
        @if($job->description)
                                        <p>{!! nl2br($job->description) !!} </p>
                                    @else
                                        <p>N/A</p>
                                    @endif
                                    <p>Job Experince</p>
                                    <p>{{ isset($job->experience) ? $job->experience .' '. __('messages.candidate_profile.year') :'No experience' }}</p>
</div>
@auth
                                    @role('Candidate')
<button class="btn btn-red btn-effect reportJobAbuse {{ ($isJobReportedAsAbuse) ? 'disabled' : '' }}"
                                                    data-toggle="modal"
                                                    data-target="#reportJobAbuseModal">{{ __('web.job_details.report_abuse') }}
                                            </button>
                                            @endrole
                                @endauth
							</div>
							<div class="col-lg-4 bg-gray">
								<div class="p-a25">
                                <h5>{{ __('web.job_details.company_overview') }}</h5>
									<ul class="list-check primary">
										<li><h5 class="text-primary"> {{ $job->company->user->first_name }}</h5></li>
										<li>@if (!empty($job->company->city_name))
                                                {{$job->company->city_name}} ,
                                            @endif

                                            @if (!empty($job->company->state_name))
                                                {{$job->company->state_name}},
                                            @endif

                                            @if (!empty($job->company->country_name))
                                                {{$job->company->country_name}}
                                            @endif

                                            @if(empty($job->company->country_name))
                                                {{ __('web.job_details.location_information_not_available') }}
                                            @endif</li>
									</ul>
                                    <a class="btn btn-outline-primary" href="#"><span class="label label-info mt20">{{ $jobsCount }} {{ __('web.companies_menu.opened_jobs') }}</span></a>
									<div class="dez-divider bg-gray-dark"></div>
								

									
									<p>  {!! nl2br($job->company->details) !!}</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
            <!-- Browse Jobs END -->
		</div>
		
    </div>
    
@endsection
@section('scripts')
    <script>
        let addJobFavouriteUrl = "{{ route('save.favourite.job') }}";
        let reportAbuseUrl = "{{ route('report.job.abuse') }}";
        let emailJobToFriend = "{{ route('email.job') }}";
        let isJobAddedToFavourite = "{{ $isJobAddedToFavourite }}";
        let removeFromFavorite = "{{ __('web.job_details.remove_from_favorite') }}";
        let addToFavorites = "{{ __('web.job_details.add_to_favorite') }}";
    </script>
    <script src="{{ mix('assets/js/jobs/front/job_details.js') }}"></script>
@endsection
