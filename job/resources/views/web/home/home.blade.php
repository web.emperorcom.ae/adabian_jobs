@extends('web.layouts.app')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')
  
    <!-- ===== Start of Main Search Section ===== -->
    @if($settings->value)
        <div class="item">
            
        </div>
    @endif
    
	<div class="dez-bnr-inr overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white">Browse Candidates</h1>
					<!-- Breadcrumb row -->
					<div class="breadcrumb-row">
						<ul class="list-inline">
							<li><a href="#">Home</a></li>
							<li>Browse Candidates</li>
						</ul>
					</div>
					<!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
		<!-- Filters Search -->
		<div class="section-full browse-job-find">
			<div class="container">
				<div class="find-job-bx">
					
					<form  class="dezPlaceAni" action="{{ route('front.search.jobs') }}" method="get">
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group">
									
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Job Title, Keywords, or Phrase">
										<div class="input-group-append">
										  <span class="input-group-text"><i class="fa fa-search"></i></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Emirate / State">
										<div class="input-group-append">
										  <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-lg-2">
								<button type="submit" class="site-button btn-block">Find Job</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
      </div>
    </div>
   <br>

	<div class="section-full job-categories content-inner-2 bg-white">
			<div class="container">
				<div class="section-head text-center">
					<h2 class="m-b5">Popular Categories</h2>
					<h5 class="fw4">20+ Catetories work wating for you</h5>
				</div>
				<div class="row sp20">
					
				@foreach($categories as $category)
 


					<div class="col-lg-3 {{ ($loop->last && $loop->iteration == 16) ? 'col-md-6 col-sm-6 ' : '' }}">
						<div class="icon-bx-wraper">
							<div class="icon-content">
								<div class="icon-md text-primary m-b20"><i class="ti-wallet"></i></div>
								<a href="{{ route('front.search.jobs',array('categories'=> $category->id)) }}" class="dez-tilte"> {{ $category->name }} </a>
								<p class="m-a0"> ( {{ $category->jobs_count }} ) Open Positions</p>
								<div class="rotate-icon"><i class="ti-wallet"></i></div> 
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		

@endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

