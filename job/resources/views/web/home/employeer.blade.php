@extends('web.layouts.Employeeapp')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')
  
    <!-- ===== Start of Main Search Section ===== -->
   
    <div class="page-content">
		<!-- Section Banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white">Employeer Job Posting</h1>
					<!-- Breadcrumb row -->
					<div class="breadcrumb-row">
						<ul class="list-inline">
							<li><a href="#">Home</a></li>
							<li>Job Posting</li>
						</ul>
                        <button class="site-button"  onclick="location.href='{{ route('front.employee.login') }}';">POST JOB</button>
						
					</div>
					<!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
		
		<!-- Section Banner END -->
        <!-- About Us -->
		<div class="section-full job-categories content-inner-2 bg-white">
			<div class="container">
			    <div class="section-head text-black text-center">
					<h2 class="text-uppercase m-b0">How It Works</h2>
				
				</div>
				
				<div class="row sp20">
				   <div class="col-lg-4">
                        <div class="card">
                           <div class="card-body">
                            <h6 style="color: #2e55fa;">1</h6>
                            <h4> Create your<br>
free account </h6>
<p>All you need is your email address to create an account and start building your job post. </p>
                            </div>
                       </div>

                    
				    </div>

                    <div class="col-lg-4">
                        <div class="card">
                           <div class="card-body">
                           <h6 style="color: #2e55fa;">2</h6>
                             <h4> Build your<br>
                             job post </h4>
                             <p> Then just add a title, description, and location to your job post, and you're ready to go.</p>
                            </div>
                       </div>

                    
				    </div>

                    <div class="col-lg-4">
                        <div class="card">
                           <div class="card-body">
                           <h6 style="color: #2e55fa;">3</h6>
                             <h4> Post<br>
                             your job</h4>
                             <p>After you post your job use our state of the art tools to help you find dream talent. </p>
                            </div>
                       </div>

                    
				    </div>
					
				</div>
			</div>
		</div>
	
		
		
		<!-- Call To Action END -->
		<!-- Our Latest Blog -->
		
		<!-- Our Latest Blog -->
	</div>
	

@endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

