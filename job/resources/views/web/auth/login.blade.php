@extends('web.layouts.apps')
@section('title')
    {{ __('web.login') }}
@endsection
@section('content')
   
<div class="page-content bg-white login-style2"  style="background-image:url(../images/background/bg6.jpg); background-size: cover;">
        <div class="section-full">
            <!-- Login Page -->
            <div class="container">
                <div class="row">
					<div class="col-lg-4 d-flex">
						<div class="text-white max-w400 align-self-center">
						<div class="logo">
                            <a href="#"><img src="../images/logo-white.png" class="logo" alt=""></a>


							</div>
							<h2 class="m-b10">Login To You Now</h2>
							<p class="m-b30">If you have Account Login Here!!</p>
							<ul class="list-inline m-a0">
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					
                    <div class="col-lg-8">
                       
                       
                            <div class="login-2 submit-resume p-a30 seth">
                                <div class="tab-content nav">
                                    <div class="tab-content">

                                    @include('flash::message')
                <!-- Start of Nav Tabs -->
                <ul class="nav nav-tabs login-nav" role="tablist" id="loginTab">

                    <!-- Candidate Tab -->
                    <li role="presentation" class="active">
                        <a href="#candidate" aria-controls="candidate" role="tab" data-toggle="tab"
                           aria-expanded="true" id="linkCandidate">
                           <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:170px;">Candidate Login</button>

                        </a>
                    </li>

                    <!-- Employer Tab -->
                    <li role="presentation" class="">
                        <a href="#employer" aria-controls="employer" role="tab" data-toggle="tab"
                           aria-expanded="false" id="linkEmpolyee">
                           <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:170px;">Employeer Login</button>
                        </a>
                    </li>
                    <br>
                </ul>
               
                                        <div role="tabpanel" class="tab-pane active" id="candidate" >
                                            <div class="login-box">
                                                  
                                                <form method="POST" action="{{ route('front.login') }}" id="candidateForm" class="tab-pane active col-12 p-a0 ">
                                                @csrf
                                <div id="candidateValidationErrBox">
                                    @include('layouts.errors')
                                </div>
                                <input type="hidden" name="type" value="1"/>
                                <div class="form-group">
                                    <label>{{ __('web.common.email') }}</label>
                                    <input type="email" name="email" class="form-control" id="email"
                                           placeholder="Your Email"
                                           value="{{ (Cookie::get('email') !== null) ? Cookie::get('email') : old('email') }}"
                                           autofocus required>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('web.common.password') }}</label>
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="Your Password"
                                           value="{{ (Cookie::get('password') !== null) ? Cookie::get('password') : null }}"
                                           required>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="checkbox" name="remember" class="custom-control-input"
                                                   id="remember" {{ (Cookie::get('remember') !== null) ? 'checked' : '' }}>
                                            <label for="remember">{{ __('web.login_menu.remember_me') }}</label>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <a href="{{ route('password.request') }}">{{ __('web.login_menu.forget_password') }}</a>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form-group text-center ml20">
                                    <div class="social-login-buttons d-flex flex-md-wrap justify-content-center">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:309px;">Login</button>
                                    </div>
                                </div>
                                                </form>




                                            </div>

                                        </div>  

                                        <div role="tabpanel" class="tab-pane" id="employer">
                                           <div class="login-box">
                                               

                                                <form method="POST" action="{{ route('front.login') }}" id="employeeForm" >
                                @csrf
                                <div id="employerValidationErrBox">
                                    @include('layouts.errors')
                                </div>
                                <input type="hidden" name="type" value="0"/>
                                <div class="form-group">
                                    <label>{{ __('web.common.email') }}</label>
                                    <input type="email" name="email" class="form-control" id="email"
                                           placeholder="Your Email"
                                           value="{{ (Cookie::get('email') !== null) ? Cookie::get('email') : old('email') }}"
                                           autofocus required>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('web.common.password') }}</label>
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="Your Password"
                                           value="{{ (Cookie::get('password') !== null) ? Cookie::get('password') : null }}"
                                           required>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="checkbox" name="remember" class="custom-control-input"
                                                   id="rememberemployer" {{ (Cookie::get('remember') !== null) ? 'checked' : '' }}>
                                            <label for="rememberemployer">{{ __('web.login_menu.remember_me') }}</label>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <a href="{{ route('password.request') }}">{{ __('web.login_menu.forget_password') }}</a>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form-group text-center ml20">
                                    <div class="social-login-buttons d-flex flex-md-wrap justify-content-center">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:309px;">Login</button>
                                    </div>
                                </div>
                            </form>
                                            </div>    


                                        </div>   





                                    </div>

                                </div>
                             </div>
                           



                    </div>



                    
				</div>
			</div>
			<!-- Login Page END -->
		</div>
		<footer class="login-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<span class="float-left">© Copyright 2020  by 
						<a href="http://emperordigital.ae/" target="_blank">Emperor Digital </a> </span>
						<span class="float-right">
							All rights reserved.
						</span>
					</div>
				</div>
			</div>
		</footer>
	</div>




    
@endsection

@section('scripts')
    <script>
        let registerSaveUrl = "{{ route('front.save.register') }}";
    </script>
    <script src="{{mix('assets/js/front_register/front_register.js')}}"></script>
@endsection