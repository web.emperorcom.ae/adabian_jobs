<?php

namespace App\Queries;

use App\Models\City;

/**
 * Class StateDataTable
 */
class CityDataTable
{
    /**
     * @return City
     */
    public function get()
    {
        /** @var City $query */
        $query = City::with('state')->select('cities.*');

        return $query;
    }
}
