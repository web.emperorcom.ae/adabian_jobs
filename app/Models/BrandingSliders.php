<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class BrandingSliders extends Model implements HasMedia
{
    use HasMediaTrait;

    const STATUS = [
        1 => 'Active',
        0 => 'Deactive',
    ];

    public const PATH = 'branding-sliders';

    public $table = "branding_sliders";
    public $fillable = [
        'title',
        'is_active',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'           => 'required',
        'branding_slider' => 'required',
    ];

    /**
     * @var array
     */
    protected $appends = ['branding_slider_url'];

    /**
     * @return mixed
     */
    public function getBrandingSliderUrlAttribute()
    {
        /** @var Media $media */
        $media = $this->media->first();
        if (! empty($media)) {
            return $media->getFullUrl();
        }

        return asset('assets/img/infyom-logo.png');
    }
}
