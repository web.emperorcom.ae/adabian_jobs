'use strict';

$(document).ready(function () {

    $('#blog_category_id').select2({
        width: '100%',
        placeholder: 'Select Post Category',
    });

    $('#description').summernote({
        minHeight: 200,
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['para', ['paragraph']],
        ],
    });

    $(document).on('submit', '#editBlogForm', () => {
        if ($('#description').summernote('isEmpty')) {
            $('#description').val('');
        }
    });

    $(document).on('change', '#image', function () {
        let validFile = isValidFile($(this), '#validationErrorsBox');
        if (validFile) {
            displayPhoto(this, '#previewImage');
            $('#btnSave').prop('disabled', false);
        } else {
            $('#btnSave').prop('disabled', true);
        }
    });

});
