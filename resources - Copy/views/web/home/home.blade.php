@extends('web.layouts.app')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')
  
    <!-- ===== Start of Main Search Section ===== -->
	<div class="dez-bnr-inr jobs-category overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">
            <div class="container">
                <div class="row">
					<div class="col-lg-8">
					<div class="dez-bnr-inr-entry"><br><br>
				       <h3 style="color: #ffff;">Find Better.Faster with Adabian</h3>
					        <div class="job-search-form">
					            <form  class="dezPlaceAni" action="{{ route('front.search.jobs') }}" method="get">
							        <div class="input-group">
								       <input type="text" class="form-control" placeholder="Job Title, Keywords">
								            <input type="text" class="form-control" placeholder="City, Province Or Region">
								        <div class="input-group-prepend">
									      <button class="site-button">Search</button>
							        	</div>
										
										<p style="color: #fff;"><br>Trending Searches :Jobs In Dubai, Jobs in Sharjah, Jobs in Abu Dhabi, Jobs in 	Ajman, Jobs in Ras Al-Khaimah</p>
							         </div>
						        </form>
			    	        </div>
				   </div>
					</div>

					
					<div class="col-lg-4 ">
					<h3 style="color: #ffff;"></h3>
							<div class="pending-info text-white p-a25" style="margin-top: 103px;">
								
								<h5>CANDIDATES</h5>
								<p>It only take a minute to Register</p>
								<a href="{{ route('candidate.register') }}" class="site-button" style="background-color: #fff;color: #212529;">UPLOAD CV </a>
							</div>
						
					</div>
				</div>
            </div>
			<!-- Modal -->
			
			<!-- Modal End -->
        </div> 








    
    <div class="section-full job-categories content-inner-2 bg-white">
			<div class="container">
				<div class="section-head text-center">
					<h2 class="m-b5">Popular Categories</h2>
					<h5 class="fw4">20+ Categories work Awaiting for you</h5>
				</div>
				<div class="row sp20">
					
				@foreach($categories as $category)
 


					<div class="col-lg-3 {{ ($loop->last && $loop->iteration == 16) ? 'col-md-6 col-sm-6 ' : '' }}">
						<div class="icon-bx-wraper">
							<div class="icon-content">
								<div class="icon-md text-primary m-b20"><i class="ti-wallet"></i></div>
								<a href="{{ route('front.search.jobs',array('categories'=> $category->id)) }}" class="dez-tilte"> {{ $category->name }} </a>
								<p class="m-a0"> ( {{ $category->jobs_count }} ) Open Positions</p>
								<div class="rotate-icon"><i class="ti-wallet"></i></div> 
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		

@endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

