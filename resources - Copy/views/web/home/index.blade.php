@extends('web.layouts.app')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')
  


    <!-- ===== Start of Main Search Section ===== -->
	<div class="dez-bnr-inr jobs-category ">
    <div class="container">
				<div class="row">
					<div class="col-lg-12 section-head text-center">
					<br><br>
						<h2 class="m-b5">We Are Adabian </h2>
						<h6 class="fw4 m-b0">Complete Solutions for your Classifieds.</h6>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-sm-6 col-md-6 m-b30">
						<a href="{{ route('front.home') }}">
							<div class="city-bx align-items-end  d-flex" style="background-image:url(images/Menubutton_Job.jpg)">
								<div class="city-info">
									<h5>Jobs</h5>
									
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6 col-md-6 m-b30">
						<a href="{{ route('web.home.Comimg-soon') }}">
							<div class="city-bx align-items-end  d-flex" style="background-image:url(images/Menubutton_RealEstate.jpg)">
								<div class="city-info">
									<h5>Realestate</h5>
									
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6 col-md-6 m-b30">
						<a href="{{ route('web.home.Comimg-soon') }}">
							<div class="city-bx align-items-end  d-flex" style="background-image:url(images/Menubutton_classified.jpg)">
								<div class="city-info">
									<h5>Classifieds</h5>
									
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6 col-md-6 m-b30">
						<a href="{{ route('web.home.Comimg-soon') }}">
							<div class="city-bx align-items-end  d-flex" style="background-image:url(images/Menubutton_Motors.jpg)">
								<div class="city-info">
									<h5>Motors</h5>
								
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6 col-md-6 m-b30">
						<a href="{{ route('web.home.Comimg-soon') }}">
							<div class="city-bx align-items-end  d-flex" style="background-image:url(images/Menubutton_Electronics.jpg)">
								<div class="city-info">
									<h5>Electronics</h5>
								
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6 col-md-6 m-b30">
						<a href="{{ route('web.home.Comimg-soon') }}">
							<div class="city-bx align-items-end  d-flex" style="background-image:url(images/Menubutton_Community.jpg)">
								<div class="city-info">
									<h5>Community</h5>
									
								</div>
							</div>
						</a>
					</div>
					
					
				</div>
			</div>
			<!-- Modal -->
			
			<!-- Modal End -->
        </div> 


        <div class="content-block">
            <div class="section-full content-inner overlay-white-middle">
				<div class="container">
					
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 m-b30">
							<div class="icon-bx-wraper p-a30 center bg-gray radius-sm">
								<div class="icon-md text-primary m-b20"> <a href="#" class="icon-cell text-primary"><i class="ti-desktop"></i></a> </div>
								<div class="icon-content">
									<h5 class="dlab-tilte text-uppercase"> 	Adabian Signature</h5>
                                    <p>We know what you wish  for </p>
									<p>Adabian.com ushers you to  effectively  manage your career,enhance lives, businesses corporations and socio economic stability around the world.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 m-b30">
							<div class="icon-bx-wraper p-a30 center bg-gray radius-sm">
								<div class="icon-md text-primary m-b20"> <a href="#" class="icon-cell text-primary"><i class="ti-image"></i></a> </div>
								<div class="icon-content">
									<h5 class="dlab-tilte text-uppercase"> 	Adabian vision</h5>
                                    <p>What we what to be  </p>
									<p>Adabian will be one of the credible leading global platform featuring wage earners to ally with appropriate jobs .<br><br></p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 m-b30">
							<div class="icon-bx-wraper p-a30 center bg-gray radius-sm">
								<div class="icon-md text-primary m-b20"> <a href="#" class="icon-cell text-primary"><i class="ti-cup"></i></a> </div>
								<div class="icon-content">
									<h5 class="dlab-tilte text-uppercase"> 	Adabian  mission</h5>
                                     <p>We say what we do & we do what we say </p>
									<p>Our resolution is to connect  professionals and  recruiters by ceding relevant information to make them more productive ,successful & satisfied. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>





    
    
		

@endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

