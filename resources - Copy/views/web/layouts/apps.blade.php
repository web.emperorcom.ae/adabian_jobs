@php
    $settings  = settings();
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="facebook-domain-verification" content="vz3wb7qai8o0w1bn1izf1l7enu4qb6" />
    <meta charset="UTF-8">
    <!-- Mobile viewport optimized -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">

    <!-- Meta Tags - Description for Search Engine purposes -->
    <meta name="description" content="{{config('app.name')}}">
    <meta name="keywords"content="{{config('app.name')}}">
    <link rel="shortcut icon" href="{{ asset($settings['favicon'])}}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Website Title -->
    <title>@yield('title') | {{config('app.name')}} </title>

    <!-- Google Fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:300,400,400i,700,800|Varela+Round" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">

    <!-- CSS links -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mains.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    
   
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/owl.carousel.min.css') }}">
   
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/fontawesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/themify/themify-icons.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/templete.min.css') }}">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ asset('css/skin/skin-1.min.css') }}">
	<link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&amp;family=Montserrat:wght@100;200;300;400;500;600;700;800;900&amp;family=Open+Sans:wght@300;400;600;700;800&amp;family=Roboto:wght@100&amp;family=Rubik:wght@300;400;500;700;900&amp;display=swap" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

   
    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
    @livewireStyles

@yield('page_css')
@yield('css')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="{{ asset('web/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('web/js/respond.min.js') }}"></script>
    <![endif]-->
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NNQX9JC');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '515760692743325');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=515760692743325&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body id="bg">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNQX9JC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="page-wraper">

<!-- Header Start -->



<!-- Header End -->

<!-- Main Content Start -->
@yield('content')
<!-- Main Content End -->
</div>
<!-- Footer Start -->

   

    </body>

<!-- Mirrored from job-board.dexignzone.com/xhtml/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Feb 2021 05:58:07 GMT -->
</html>
<!-- Footer End -->

<!-- ===== All Javascript at the bottom of the page for faster page loading ===== -->
<script src="{{ asset('js/combining.js') }}"></script>
<script src="{{ asset('web/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('web/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('web/js/swiper.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('web/js/jquery.inview.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('web/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('web/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('web/js/countdown.js') }}"></script>
<script src="{{ asset('web/js/isotope.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ asset('web/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script>
    (function ($) {
        $.fn.button = function (action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }(jQuery));
    $(document).ready(function () {
        $('.alert').delay(5000).slideUp(300);
    });
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data('target') || $t.parents('.modal') || [];

        $(target).modal('hide');
    });
    let createNewLetterUrl = "{{ route('news-letter.create') }}";
</script>
<script src="{{ mix('assets/js/web/js/news_letter/news_letter.js') }}"></script>
<script src="//code.tidio.co/reobslcjkejbjdrep3qyo2dkaengw1ge.js" async></script>
@livewireScripts


@yield('page_scripts')
@yield('scripts')
</body>
</html>
