@extends('web.layouts.Employeeapp')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')
  
    <!-- ===== Start of Main Search Section ===== -->
   
    <div class="page-content">
		<!-- Section Banner -->
        <div class="overlay-black-dark profile-edit p-t50 p-b20" style="background-image:url(../images/employeerbanner.png);">
            <div class="container">
                <div class="row">
					<div class="col-lg-4">
						<div class="candidate-detail">
							
								
								
							
							
						</div>
					</div>

					<div class="col-lg-4 ">
						<div class="candidate-detail">
							
							<div class="text-white browse-job text-left">
								
								
								<ul class="clearfix">
									<li><img src="https://media.monsterindia.com/recruiter_2015/india/images/icon-bg.png"/><h6 style="font-size: 22px;padding-left: 35px;margin-top: -23px;">AI in Recruiting Technology</h6></li>
									<li><img src="https://media.monsterindia.com/recruiter_2015/india/images/icon-bg.png"/><h6 style="font-size: 22px;padding-left: 35px;margin-top: -23px;">Conversational UI</h6></li>
									<li><img src="https://media.monsterindia.com/recruiter_2015/india/images/icon-bg.png"/><h6 style="font-size: 22px;padding-left: 35px;margin-top: -23px;">Data Security</h6></li>
									<li><img src="https://media.monsterindia.com/recruiter_2015/india/images/icon-bg.png"/><h6 style="font-size: 22px;padding-left: 35px;margin-top: -23px;">Power Recruiting</h6></li>
								</ul>
								
							</div>
						</div>
					</div>
					<div class="col-lg-4 ">
					
							<div class="pending-info text-white p-a25">
								
								<h5>EMPLOYER</h5>
								<p>Find the next candidate for free</p>
								<a href="http://adabian.com/employer-register" class="site-button" style="background-color: #fff;color: #212529;">POST A JOB </a>
							</div>
						
					</div>
				</div>
            </div>
			<!-- Modal -->
			<div class="modal fade browse-job modal-bx-info editor" id="profilename" tabindex="-1" role="dialog" aria-labelledby="ProfilenameModalLongTitle" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="ProfilenameModalLongTitle">Basic Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form>
								<div class="row">
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Your Name</label>
											<input type="email" class="form-control" placeholder="Enter Your Name">
										</div>
									</div>
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-6">
													<div class="custom-control custom-radio">
														<input type="radio" class="custom-control-input" id="fresher" name="example1">
														<label class="custom-control-label" for="fresher">Fresher</label>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-6">
													<div class="custom-control custom-radio">
														<input type="radio" class="custom-control-input" id="experienced" name="example1">
														<label class="custom-control-label" for="experienced">Experienced</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="form-group">
											<label>Select Your Country</label>
											<select>
												<option>India</option>
												<option>Australia</option>
												<option>Bahrain</option>
												<option>China</option>
												<option>Dubai</option>
												<option>France</option>
												<option>Germany</option>
												<option>Hong Kong</option>
												<option>Kuwait</option>
											</select>
										</div>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="form-group">
											<label>Select Your Country</label>
											<input type="text" class="form-control" placeholder="Select Your Country">
										</div>
									</div>
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Select Your City</label>
											<input type="text" class="form-control" placeholder="Select Your City">
										</div>
									</div>
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Telephone Number</label>
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-4">
													<input type="text" class="form-control" placeholder="Country Code">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-4 col-4">
													<input type="text" class="form-control" placeholder="Area Code">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-4 col-4">
													<input type="text" class="form-control" placeholder="Phone Number">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Email Address</label>
											<h6 class="m-a0 font-14">info@example.com</h6>
											<a href="#">Change Email Address</a>
										</div>		
									</div>		
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="site-button" data-dismiss="modal">Cancel</button>
							<button type="button" class="site-button">Save</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal End -->
        </div>
		
		<!-- Section Banner END -->
        <!-- About Us -->
		<div class="section-full job-categories content-inner-2 bg-white">
			<div class="container">
			    <div class="section-head text-black text-center">
					<h2 class="text-uppercase m-b0">How It Works</h2>
				
				</div>
				
				<div class="row sp20">
				   <div class="col-lg-4">
                        <div class="card">
                           <div class="card-body">
                            <h6 style="color: #2e55fa;">Step 1</h6>
                            <h4> Create an Account <br>
							for Free </h6>
<p>All you need is a valid email id to create your free career profile with Adabian. <br><br></p>
                            </div>
                       </div>

                    
				    </div>

                    <div class="col-lg-4">
                        <div class="card">
                           <div class="card-body">
                           <h6 style="color: #2e55fa;">Step 2</h6>
                             <h4>Create your <br>
                             Job Post </h4>
                             <p> Specify your vacany,describe your requirements,location of your job & then you are ready to go!!!</p>
                            </div>
                       </div>

                    
				    </div>

                    <div class="col-lg-4">
                        <div class="card">
                           <div class="card-body">
                           <h6 style="color: #2e55fa;">Step 3</h6>
                             <h4> Post<br>
                             your job</h4>
                             <p>After posting,you may use of state of the art tools to your help & find real talents at ease.<br><br></p>
                            </div>
                       </div>

                    
				    </div>
					
				</div>
                <div class="section-full job-categories content-inner-2 bg-white" style="
    margin-top: -32px;
">
			<div class="container">
			    <div class="section-head text-black text-center">
                <button class="site-button"  onclick="location.href='{{ route('front.employee.login') }}';">GET STARTED</button>
				
				</div>
				
				
			</div>
		</div>
			</div>
		</div>


        
	
		
		
		<!-- Call To Action END -->
		<!-- Our Latest Blog -->
		
		<!-- Our Latest Blog -->
	</div>
	

@endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

