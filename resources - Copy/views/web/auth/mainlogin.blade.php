@extends('web.layouts.apps')
@section('title')
    {{ __('web.login') }}
@endsection
@section('content')
   
<div class="page-content bg-white login-style2"  style="background-image:url(../images/background/bg6.jpg); background-size: cover;">
        <div class="section-full">
            <!-- Login Page -->
            <div class="container">
                <div class="row">
					<div class="col-lg-4 d-flex">
						<div class="text-white max-w400 align-self-center">
						<div class="logo">
                            <a href="#"><img src="../images/logo-white.png" class="logo" alt=""></a>


							</div>
							<h2 class="m-b10">Login To You Now</h2>
							<p class="m-b30">If you have Account Login Here!!</p>
							<ul class="list-inline m-a0">
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					
                    <div class="col-lg-8">
                       
                    @include('flash::message')
                            <div class="login-2 submit-resume p-a30 seth">
                               <div class="row">
  <div class="col-lg-6 ">
  <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size:60%;border-radius:0rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:160px;">Candidate Login</button>
  
  </div>
  
  <div class="col-lg-6 "><button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size:60%;border-radius:0rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;width:160px;">Employeer Login</button></div>

  <!-- Force next columns to break to new line at md breakpoint and up -->
  
</div>

                                </div>
                             </div>
                           



                    </div>



                    
				</div>
			</div>
			<!-- Login Page END -->
		</div>

        
		<footer class="login-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<span class="float-left">© Copyright 2020  by 
						<a href="http://emperordigital.ae/" target="_blank">Emperor Digital </a> </span>
						<span class="float-right">
							All rights reserved.
						</span>
					</div>
				</div>
			</div>
		</footer>
	</div>




    
@endsection

@section('scripts')
    <script>
        let registerSaveUrl = "{{ route('front.save.register') }}";
    </script>
    <script src="{{mix('assets/js/front_register/front_register.js')}}"></script>
@endsection