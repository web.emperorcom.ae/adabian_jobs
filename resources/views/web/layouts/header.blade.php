

<header class="site-header mo-left header fullwidth">
		<!-- main header -->
        <div class="sticky-header main-bar-wraper navbar-expand-lg">
            <div class="main-bar clearfix">
                <div class="container clearfix">
                    <!-- website logo -->
                    <div class="logo-header mostion">
                    <a href="http://adabian.com/"><img src="http://adabian.com/images/for-candidates.png" class="logo" alt=""></a>
					</div>
                    <!-- nav toggle button -->
                    <!-- nav toggle button -->
                    <button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
                    <!-- extra nav -->

                 
                    <div class="extra-nav">
                    
                        <div class="dez-post-readmore" style="
    color: #000;
"> 
                        @auth
                        <div class="header-nav navbar-collapse collapse justify-content-start" id="navbarNavDropdown">
                    <ul class="nav navbar-nav" style="margin-top: -20px;padding-right: 31px;">
							
						
							
							<li>
								<a href="#"><span class="d-sm-none d-lg-inline-block">
                                        Hi, {{getLoggedInUser()->full_name}}</span> <i class="fa fa-chevron-down"></i></a>
								<ul class="sub-menu">
								
                                    <li class="dez-page">
                                        <a href="{{ dashboardURL() }}">{{ __('web.go_to_dashboard') }}</a>
                                    </li>
                                    @auth
                                        @role('Candidate')
                                    <li>  <a href="{{ route('candidate.profile') }}">{{ __('web.my_profile') }}</a></li>
									<li> <a href="{{ route('favourite.jobs') }}">{{ __('messages.favourite_jobs') }}</a></li>
									<li> <a href="{{ route('favourite.companies') }}">{{ __('messages.candidate_dashboard.followings') }}</a></li>
									<li> <a href="{{ route('candidate.applied.job') }}">{{ __('messages.applied_job.applied_jobs') }}</a></li>
                                    <li> <a href="{{ route('candidate.job.alert') }}">{{ __('messages.job.job_alert') }}</a></li>
                                                                            @endrole
                                        @role('Employer')
                                        <li class="dez-page">
                                            <a href="{{ route('company.edit.form', \Illuminate\Support\Facades\Auth::user()->owner_id) }}">{{ __('web.my_profile') }}</a>
                                        </li>
                                        <li> <a href="{{ route('job.index') }}">{{ __('messages.employer_menu.jobs') }}</a></li>
                                        <li><a href="{{ route('followers.index') }}">{{ __('messages.employer_menu.followers') }}</a></li>
                                        <li><a href="{{ route('manage-subscription.index') }}">{{ __('messages.employer_menu.manage_subscriptions') }}</a></li>
                                        <li> <a href="{{ route('transaction.index') }}">{{ __('messages.employer_menu.transactions') }}</a></li>
                                        @endrole
                                    @endauth
                                    <li>
                                        <a href="{{ url('logout') }}"
                                           onclick="event.preventDefault(); localStorage.clear();  document.getElementById('logout-form').submit();">
                                            {{ __('web.logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                              class="d-none">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
								</ul>
							</li>
						</ul>		
                    </div>
                        @else
                            
                       
                       

                            
                            
                        









                        
									
                                    
                                    </div>
                      
                    </div>
                    <!-- main nav -->
                    <div class="header-nav navbar-collapse collapse justify-content-start" id="navbarNavDropdown">
                        <ul class="nav navbar-nav">
							<li class="active">
								<a href="{{ route('web.home') }}">Home </a>
								
							</li>
							<li>
								<a href="{{ route('front.home') }}">Jobs</a>
								
							</li>
						
								<li>
								<a href="{{ route('web.home.Comimg-soon') }}">Realestate</a>
								
							</li>
					
							
							<li>
								<a href="{{ route('web.home.Comimg-soon') }}">Classifieds</a>
								
							</li>
                            <li>
								<a href="{{ route('web.home.Comimg-soon') }}">Motors</a>
								
							</li>
                          
                            <li>
                            <a href="{{ route('front.candidate.login') }}"  rel="bookmark" class="site-button-link" ><span class="fw6" style="
    
    padding-left: 5px;
    color: #0275d8;

"> Login</span></a>   

<a href="{{ route('candidate.register') }}"  rel="bookmark" class="site-button-link" ><span class="fw6" style="
    
    padding-left: 5px;
    color: #0275d8;

"> Register</span></a>   
                        
									
										
                  
                    
                 
                    <a href="{{ route('web.home.employeer') }}"  rel="bookmark" class="site-button-link"><span class="fw6" style="
    
    padding-left: 5px;
    color: #0275d8;

"> For Employer</span></a>
</li>

							
						</ul>	
                        @endauth		
                    </div>



                </div>
            </div>
        </div>
       
    </header>


 