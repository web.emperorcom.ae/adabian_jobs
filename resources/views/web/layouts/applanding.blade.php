@php
    $settings  = settings();
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="facebook-domain-verification" content="vz3wb7qai8o0w1bn1izf1l7enu4qb6" />
    <meta charset="UTF-8">
    <!-- Mobile viewport optimized -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">

    <!-- Meta Tags - Description for Search Engine purposes -->
    <meta name="description" content="{{config('app.name')}}">
    <meta name="keywords"content="{{config('app.name')}}">
    <link rel="shortcut icon" href="{{ asset($settings['favicon'])}}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Website Title -->
    <title>@yield('title') | {{config('app.name')}} </title>

    <!-- Google Fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:300,400,400i,700,800|Varela+Round" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">

    <!-- CSS links -->
    <link href="{{ asset('css1/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href=" {{ asset('css1/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href=" {{ asset('css1/line.css') }}" rel="stylesheet" type="text/css" />
       
        <!-- Magnific -->
        <link href="{{ asset('css1/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
        <!-- Main Css -->
        <link href="{{ asset('css1/style.min.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="{{ asset('css1/default.css') }} " rel="stylesheet" id="color-opt">

   
    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
    @livewireStyles

@yield('page_css')
@yield('css')

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NNQX9JC');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '515760692743325');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=515760692743325&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body >

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNQX9JC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@include('web.layouts.headerlanding')






@yield('content')




   

    </body>


</html>




<script src="{{ asset('js1/bootstrap.bundle.min.js') }}"></script>
        <!-- Magnific Popup -->
        <script src="{{ asset('js1/jquery.magnific-popup.min.html') }}"></script>
        <script src="{{ asset('js1/magnific.init.html') }}"></script>
        <!-- Icons -->
        <script src="{{ asset('js1/countdown.init.js') }}"></script>
        <script src="{{ asset('js1/feather.min.js') }}"></script>
        <!-- Switcher -->
        <script src="{{ asset('js1/switcher.js') }}"></script>
        <!-- Main Js -->
        <script src="{{ asset('js1/app.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script src="{{ mix('assets/js/custom/custom.js') }}"></script>
<script>
    (function ($) {
        $.fn.button = function (action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }(jQuery));
    $(document).ready(function () {
        $('.alert').delay(5000).slideUp(300);
    });
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data('target') || $t.parents('.modal') || [];

        $(target).modal('hide');
    });
    let createNewLetterUrl = "{{ route('news-letter.create') }}";
</script>
<script src="{{ mix('assets/js/web/js/news_letter/news_letter.js') }}"></script>
<script src="//code.tidio.co/reobslcjkejbjdrep3qyo2dkaengw1ge.js" async></script>
@livewireScripts


@yield('page_scripts')
@yield('scripts')
</body>
</html>
