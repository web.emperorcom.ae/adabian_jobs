@extends('web.layouts.apps')
@section('title')
    {{ __('web.login') }}
@endsection
@section('content')
   
<div class="page-content bg-white login-style2"  style="background-image:url(images/background/bg6.jpg); background-size: cover;">
        <div class="section-full">
            <!-- Login Page -->
            <div class="container">
                <div class="row">
					<div class="col-lg-5 d-flex">
						<div class="text-white max-w400 align-self-center">
						<div class="logo">
                            <a href="{{ route('front.home') }}"><img src="images/logo-white.png" class="logo" alt=""></a>


							</div>
							<h2 class="m-b10">Sign up To You Now</h2>
							<p class="m-b30">If you have Account	<a href="{{ route('front.candidate.login') }}"  style="color: white;"> Click Here</a></p>
               <div class="Sign-footer">
              
            <p>By creating an account, you agree to Adabian  Terms of Service, Cookie Policy and Privacy Policy. You consent to receiving marketing messages from Adabian  and may opt out from receiving such messages by following the unsubscribe link in our messages, or as detailed in our terms.</p>
</div>




							<ul class="list-inline m-a0">
								<li><a href="https://www.facebook.com/adabiancom/" class="m-r10 text-white"><i class="fa fa-facebook"></i></a></li>
							
								<li><a href="https://www.linkedin.com/company/adabiancom/" class="m-r10 text-white"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="https://www.instagram.com/adabiancom/" class="m-r10 text-white"><i class="fa fa-instagram"></i></a></li>
								<li><a href="https://twitter.com/adabiancom" class="m-r10 text-white"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-7">
						<div class="sign-2 submit-resume p-a30 seth">
							<div class="tab-content nav">
              <h5 style="float: left;">Create an Account (it's free)</h5>
              <hr>
              {{ Form::open(['id'=>'addCandidateNewForm']) }}
                        <input type="hidden" name="type" value="1">
                        @csrf
									<div class="row">
										<div class="col-lg-6 col-md-6">
											<div class="form-group">
												<label>First Name</label>
                        <input type="text" name="first_name" id="candidateFirstName" class="form-control" placeholder="First Name">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror




											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="form-group">
												<label>Last Name</label>
                        <input type="text" name="last_name" id="candidateLastName" class="form-control" placeholder="Last Name">

@error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror

												
											</div>
										</div>

										<div class="col-lg-6 col-md-6">
											<div class="form-group">
												<label>Mobile Number</label>
                        <input type="text" name="phone" id="candidateFirstName" class="form-control" placeholder="0555555555" >

@error('phone')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror

												
											</div>
										</div>
										
										
										<div class="col-lg-6 col-md-6">
											<div class="form-group">
												<label>Email</label>
                        <input type="email" name="email" id="candidateEmail" class="form-control"  placeholder="Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
											</div>
										</div>
                    <div class="col-lg-6 col-md-6">
											<div class="form-group">
												<label>Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="New Password" required="required">

@error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror





											</div>
										</div>
                    <div class="col-lg-6 col-md-6">
											<div class="form-group">
												<label>Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" placeholder="Confirm Password" required="required">


											</div>
										</div>

                    <div class="col-lg-6 col-md-6">
											<div class="form-group">
                      <div class="form-check" style="float: left;">
												<label></label>

                        <input type="checkbox" class="form-check-input" name="privacyPolicy" id="exampleCheck1" checked readonly="readonly">
                        <label class="form-check-label" for="invalidCheck">
                    Agree to terms and conditions
                  </label>
                      </div>
											</div>
										</div>
              
									</div>
                  <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;transition: all 0.2s;">Create Account</button>
                  <hr>
          
                </form>
 


								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Login Page END -->
		</div>
		
	</div>


    
@endsection

@section('scripts')
    <script>
        let registerSaveUrl = "{{ route('front.save.register') }}";
        let candidateLogInUrl = "{{ route('front.candidate.login') }}";
    </script>
    <script src="{{mix('assets/js/front_register/front_register.js')}}"></script>
@endsection
