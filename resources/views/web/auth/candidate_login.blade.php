@extends('web.layouts.apps')
@section('title')
    {{ __('web.login') }}
@endsection
@section('content')
   
<div class="page-content bg-white login-style2"  style="background-image:url(../images/background/bg6.jpg); background-size: cover;">
        <div class="section-full">
            <!-- Login Page -->
            <div class="container">
                <div class="row">
					<div class="col-lg-6 col-md-6 d-flex">
						<div class="text-white max-w400 align-self-center">
						<div class="logo">
                            <a href="{{ route('front.home') }}"><img src="../images/logo-white.png" class="logo" alt=""></a>


							</div>
							<h2 class="m-b10">Login To You Now</h2>
							<p class="m-b30">If you have Account Login Here!!</p>
							<ul class="list-inline m-a0">
							<li><a href="https://www.facebook.com/adabiancom/" class="m-r10 text-white"><i class="fa fa-facebook"></i></a></li>
							
							<li><a href="https://www.linkedin.com/company/adabiancom/" class="m-r10 text-white"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="https://www.instagram.com/adabiancom/" class="m-r10 text-white"><i class="fa fa-instagram"></i></a></li>
							<li><a href="https://twitter.com/adabiancom" class="m-r10 text-white"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="login-2 submit-resume p-a30 seth">
@include('flash::message')
							<div class="tab-content nav">
								<form action="{{ route('front.login') }}" id="candidateForm" class="tab-pane active col-12 p-a0 "  method="POST">
                @csrf
               
                <div id="candidateValidationErrBox">
                                    @include('layouts.errors')
                                </div>
                                <input type="hidden" name="type" value="1"/>
                
              
									<div class="form-group">
										<label>E-Mail Address*</label>
										<div class="input-group">
											<input name="email" class="form-control" placeholder="Your Email Address" id="email" type="email"   value="{{ (Cookie::get('email') !== null) ? Cookie::get('email') : old('email') }}"
                                           autofocus >

                                           @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
										</div>
									</div>
									<div class="form-group">
										<label>Password *</label>
										<div class="input-group">
										<input type="password" name="password" class="form-control" id="password"
                                           placeholder="Your Password"
                                           value="{{ (Cookie::get('password') !== null) ? Cookie::get('password') : null }}"
                                           required>

           @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
										</div>
									</div>
                 
                 	<div class="form-group">
                      <div class="form-check" style="float: left;">
												<label></label>

                        <input type="checkbox" class="form-check-input" name="privacyPolicy" id="exampleCheck1" checked disabled="disabled">
                        <label class="form-check-label" for="invalidCheck">Agree to terms and conditions</label>
                      </div>
											</div>
         <br><br>


									<div class="text-center">
										<button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;
         border-radius: 5rem;
         letter-spacing: .1rem;
         font-weight: bold;
         padding: 1rem;
         transition: all 0.2s;">login</button>
										<a  href="{{ route('password.request') }}" class="site-button-link forget-pass m-t15 float-right"><i class="fa fa-unlock-alt"></i> Forgot Password</a> 
									
                 
                    
                  
                  </div>
                  <hr>
                 
                  
                  
                   <p class="font-weight-600" style="
    text-align: center;
">New to Account?	</p>
                  <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase" style="font-size: 80%;border-radius: 5rem;letter-spacing: .1rem;font-weight: bold;padding: 1rem;
transition: all 0.2s;" onclick="location.href='{{ route('candidate.register') }}';">Create Account</button>
                  

                 
         
								</form>
								<form id="forgot-password" class="tab-pane fade  col-12 p-a0">
									<p>We will send you an email to reset your password. </p>
									<div class="form-group">
										<label>E-Mail address *</label>
										<div class="input-group">
											<input name="dzName" required="" class="form-control" placeholder="Your Email Address" type="email">
										</div>
									</div>
									<div class="text-left"> 
										<a class="site-button outline gray" data-toggle="tab" href="#login">Back</a>
										<button class="site-button pull-right">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Login Page END -->
		</div>
		
	</div>




    
@endsection

@section('scripts')
    <script>
        let registerSaveUrl = "{{ route('front.save.register') }}";
    </script>
    <script src="{{mix('assets/js/front_register/front_register.js')}}"></script>
@endsection
