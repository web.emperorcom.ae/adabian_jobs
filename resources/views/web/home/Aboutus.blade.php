@extends('web.layouts.apps')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')

    <!-- header END -->
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white">About Us</h1>
					<!-- Breadcrumb row -->
					<div class="breadcrumb-row">
						<ul class="list-inline">
							<li><a href="#">Home</a></li>
							<li>About Us</li>
						</ul>
					</div>
					<!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
		<div class="content-block">
            <div class="section-full content-inner overlay-white-middle">
				<div class="container">
					<div class="row align-items-center m-b50">
						<div class="col-md-12 col-lg-6 m-b20">
							<h2 class="m-b5">About Us</h2>
							<h3 class="fw4">We create unique experiences</h3>
							<p class="m-b15"> Adabian.com is a reliable chaperon which assist to link your professional portrait  with the employer and viceversa.Whether you be a newborn or a veteran  in the job market,we help you to find the appropriate & authentic employers to connect with.</p>
							<p class="m-b15">Adabian.com facilitate the employer to find the right candidate to manage your organizational requirements.</p>
							<p class="m-b15">Adabian.com provides online employment solution for job seekers and recruiters at FREE OF COST!!! </p>
						</div>
						<div class="col-md-12 col-lg-6">
							<img src="images/our-work/pic1.jpg" alt=""/>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 m-b30">
							<div class="icon-bx-wraper p-a30 center bg-gray radius-sm">
								<div class="icon-md text-primary m-b20"> <a href="#" class="icon-cell text-primary"><i class="ti-desktop"></i></a> </div>
								<div class="icon-content">
									<h5 class="dlab-tilte text-uppercase"> 	Adabian Signature</h5>
                                    <p>we know what you wish  for </p>
									<p>Adabian.com ushers you to  effectively  manage your career,enhance lives, businesses corporations and socio economic stability around the world.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 m-b30">
							<div class="icon-bx-wraper p-a30 center bg-gray radius-sm">
								<div class="icon-md text-primary m-b20"> <a href="#" class="icon-cell text-primary"><i class="ti-image"></i></a> </div>
								<div class="icon-content">
									<h5 class="dlab-tilte text-uppercase"> 	Adabian vision</h5>
                                    <p>What we what to be  </p>
									<p>Adabian will be one of the credible leading global platform featuring wage earners to ally with appropriate jobs .<br><br></p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 m-b30">
							<div class="icon-bx-wraper p-a30 center bg-gray radius-sm">
								<div class="icon-md text-primary m-b20"> <a href="#" class="icon-cell text-primary"><i class="ti-cup"></i></a> </div>
								<div class="icon-content">
									<h5 class="dlab-tilte text-uppercase"> 	Adabian  mission</h5>
                                     <p>we say what we do & we do what we say </p>
									<p>Our resolution is to connect  professionals and  recruiters by ceding relevant information to make them more productive ,successful & satisfied. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Why Chose Us -->
			<!-- Call To Action -->
			<div class="section-full content-inner-2 call-to-action overlay-black-dark text-white text-center bg-img-fix" style="background-image: url(images/background/bg4.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="m-b10">Make a Difference with Your Online Resume!</h2>
							<p class="m-b0">We provide quality artisan profiles to fill your vacant chairs,</p>
							<a href="{{ route('candidate.register') }}" class="site-button m-t20 outline outline-2 radius-xl">Create an Account</a>
						</div>
					</div>
				</div>
			</div>
            <div class="section-full content-inner overlay-white-middle">
				<div class="container">
					<div class="row align-items-center m-b50">
                    <div class="col-md-12 col-lg-6">
							<img src="images/our-work/pic1.jpg" alt=""/>
						</div>
						<div class="col-md-12 col-lg-6 m-b20">
							<h2 class="m-b5">  	Why Adabia.com?</h2>
							<p class="m-b0">We provide quality artisan profiles to fill your vacant chairs,</p><br>
							<p class="m-b15">We believe- “When a crafts person is looking for a job opportunity, there will be an employer on the other end looking for a talented crafts man to work for them.”</p>
							<p class="m-b15">We help you make smarter decisions to improve your ROI by providing quality profiles in ample quantity. With our leading edge technology;sorting,analayzing & finalizing are at ease managing your time & space effectively. </p>
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- Call To Action END -->
			<!-- Our Latest Blog -->
			
			<!-- Our Latest Blog -->
        </div>
		<!-- contact area END -->
    </div>
    <!-- Content END-->
	<!-- Modal Box -->
	<div class="modal fade lead-form-modal" id="car-details" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="modal-body row m-a0 clearfix">
					<div class="col-lg-6 col-md-6 overlay-primary-dark d-flex p-a0" style="background-image: url(images/background/bg3.jpg);  background-position:center; background-size:cover;">
						<div class="form-info text-white align-self-center">
							<h3 class="m-b15">Login To You Now</h3>
							<p class="m-b15">Lorem Ipsum is simply dummy text of the printing and typesetting industry has been the industry.</p>
							<ul class="list-inline m-a0">
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#" class="m-r10 text-white"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 p-a0">
						<div class="lead-form browse-job text-left">
							<form>
								<h3 class="m-t0">Personal Details</h3>
								<div class="form-group">
									<input value="" class="form-control" placeholder="Name"/>
								</div>	
								<div class="form-group">
									<input value="" class="form-control" placeholder="Mobile Number"/>
								</div>
								<div class="clearfix">
									<button type="button" class="btn-primary site-button btn-block">Submit </button>
								</div>
							</form>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<!-- Modal Box End -->
	<!-- Footer -->
    @endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

