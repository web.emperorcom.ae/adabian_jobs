@extends('web.layouts.apps')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')

    <!-- header END -->
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white">Privacy and Policy</h1>
					<!-- Breadcrumb row -->
					<div class="breadcrumb-row">
						<ul class="list-inline">
							<li><a href="#">Home</a></li>
							<li>Privacy and Policy</li>
						</ul>
					</div>
					<!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
		<div class="content-block">
            <div class="section-full content-inner overlay-white-middle">
				<div class="container">
					<div class="row align-items-center m-b50">
						<div class="col-md-12 col-lg-12 m-b20">
						  <h5>Our Commitment and Terminology</h5>
						
							<p class="m-b15">
Adabian is committed to protecting the rights and privacy of all visitors to its websites and/or mobile applications.<br>
The terms "we", "us" or "our(s)" hereunder refer to Adabian and its affiliates and the terms "you" and "your" hereunder refer to the user visiting  Adabian.com Careers portal. 
</p>
							<h3 class="fw4"> 	Scope of Policy</h3>
                            <p class="m-b15">This privacy policy describes how Adabian.com collects and uses the personal information you provide. It also describes the choices available to you regarding our use of your personal information and how you can access and update this information. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. <br>In the event of any inconsistency or conflict between the Website Terms and this privacy policy, the terms of this privacy policy shall prevail.
The Company may revise these Terms and Conditions at any time by updating this posting. You should visit this page periodically to review the Terms and Conditions, because they are binding on you.
 </p>
 <h3 class="fw4">   	Collection and Use</h3>
 <p class="m-b15" style=" text-align: justify;">
 We also collect the following information from you:
    <br>•         Information you give us (Submitted information)
You may give us information about you by filling in forms on the Services Sites or by corresponding with us (for example, by e-mail). This includes information you provide when you register to use the Site, and when you report a problem with Adabian jobs, our Services, or any of Our Sites. The information you give us may include your name, address, e-mail address and phone number, the Device's phone number, age, username, password and other registration information, and personal description.
 
<br>•         Information we collect about you and your device 
Each time you visit one of Our Sites or avail our services we may automatically collect the following information:
 
    <br>•         technical information, including the type of mobile device you use, a unique device identifier (for example, your Device's IMEI number, the MAC address of the Device's wireless network interface, or the mobile phone number used by the Device), mobile network information, your mobile operating system, the type of mobile browser you use, time zone setting, IP address and referring/exit pages (Device Information);
 
    <br>•         details of your use of Adabian Jobs or your visits to any of Our Sites (including, but not limited to, traffic data, location data, weblogs and other communication data, whether this is required for our own applying for job purposes or otherwise) and the resources that you access (Log Information).

 </p>




 <h3 class="fw4">  	Unique application numbers</h3>
 <p class="m-b15" style="text-align: justify;">When you install or uninstall a Service containing a unique application number or when such a Service searches for automatic updates, that number and information about your installation (for example, the type of operating system) may be sent to us.<br>
 We use information collected to:<br>
•                Fulfil your requests;<br>
•                Send you a request confirmation;<br>
•                Assess your needs to determine suitable products or services;<br>
•                Send you requested service information;<br>
•                Send service updates or warranty information;<br>
•                Respond to customer service requests;<br>
•                Administer your account;device or Dubai Careers settings depending on your device type. You can also contact us at info@adabian.com<br>
•                Send you marketing communications;<br>
•                Improve our Site Services  and marketing efforts;<br>
•                Conduct research and analysis;<br>
•                Respond to your questions and concerns; and<br>
•                Display content based upon your interests.<br>
 

 </p>







 <h3 class="fw4">  Cookies</h3>
 <p class="m-b15" style="text-align: justify;
">We may use cookies to distinguish you from other users of Adabian.com. This helps us to provide you with a good experience when you use Adabian.com or browse any of the Sites and also allows us to improve our sites. </p>
 

<h3 class="fw4" >   	Sharing Your Information</h3>
 <p class="m-b15" style="text-align: justify;">We will share your information with third parties only in the ways that are described in this privacy policy. We may provide your personal information to affiliate companies that provide services to help us with our business activities such as offering customer service. These third parties are authorized to use your personal information only as necessary to provide these Services to us.</p>
 <h3 class="fw4">  	We may disclose your personal information</h3>
 <p class="m-b15" style="text-align: justify;">• As required by law, such as to comply with a court order or any other legal process;
<br>• when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request; and/or
<br>• To any other third party with your prior consent to do so.
</p>



<h3 class="fw4">  	We may disclose your personal information</h3>
 <p class="m-b15" style="text-align: justify;">• As required by law, such as to comply with a court order or any other legal process;
<br>• when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request; and/or
<br>• To any other third party with your prior consent to do so.
</p>




<h3 class="fw4">    	Security</h3>
 <p class="m-b15" style="text-align: justify;">The security of your personal information is important to us. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. 

</p>

<p class="m-b15" style="text-align: justify;">Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to Our Sites; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorized access.

</p>

<p class="m-b15" style="text-align: justify;">We will retain your information for as long as your account is active or as needed to provide you with Services. If you wish to cancel your account or request that we no longer use your information to provide you with Services please contact us at info@adabian.com
 We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.


</p>

<p class="m-b15" style="text-align: justify;">Where we have given you (or where you have chosen) a password that enables you to access certain parts of Our Sites, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.

</p>

<h3 class="fw4">  	 	Notification of Privacy Policy Changes</h3>
 <p class="m-b15" style="text-align: justify;">We may update this privacy policy to reflect changes to our information practices or any relevant statute, regulations and/or policy. We encourage you to periodically review this page for the latest information on our privacy practices.
</p>


<h3 class="fw4">  	Contact</h3>
 <p class="m-b15" style="text-align: justify;">You can contact us about this privacy policy by writing or email us at the address below:
<p>Adabian.com</p>
<p>Telephone number: </p>
<p>Fax number: </p>
<p>Address: Dubai, United Arab Emirates</p>
<p>Email: info@adabian.com</p>
</p>
 
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- Why Chose Us -->
			<!-- Call To Action -->
			
			<!-- Call To Action END -->
			<!-- Our Latest Blog -->
			
			<!-- Our Latest Blog -->
        </div>
		<!-- contact area END -->
    </div>
    <!-- Content END-->
	<!-- Modal Box -->
	
	<!-- Modal Box End -->
	<!-- Footer -->
    @endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

