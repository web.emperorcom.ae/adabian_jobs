@extends('layouts.app')
@section('title')
    {{ __('messages.candidates') }}
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>{{ __('messages.candidates') }}</h1>
            <div class="section-header-breadcrumb">
                <div class="row justify-content-end">
                    <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
                        <div class="card-header-action">
                            {{  Form::select('is_immediate_available', $immediateAvailable, null, ['id' => 'immediateAvailable', 'class' => 'form-control status-filter w-100', 'placeholder' => 'Select Immediate Available']) }}
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12 mt-3 mt-md-0 ml-4">
                        <div class="card-header-action w-100">
                            {{  Form::select('is_status', $statusArr, null, ['id' => 'filter_status', 'class' => 'form-control status-filter w-100', 'placeholder' => 'Select Status']) }}
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12 mt-md-1 pr-0">

                        <div class="dropdown candidate-index__action d-inline">
                            <button class="btn btn-primary dropdown-toggle" type="button"
                                    data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">{{__('messages.common.action')}}
                            </button>
                            <div class="dropdown-menu candidate-index__menu">
                                <a class="dropdown-item has-icon" href="{{ route('candidates.create') }}"><i
                                            class="fas fa-plus"></i> {{ __('messages.common.add') }}</a>
                                <a class="dropdown-item has-icon" href="{{ route('candidates.export.excel') }}"><i
                                            class="far fa-file"></i> Export Excel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('flash::message')
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('candidates.table')
                </div>
            </div>
        </div>
        @include('candidates.templates.templates')
    </section>
@endsection
@push('scripts')
    <script>
        let candidateUrl = "{{ route('candidates.index') }}";
        let immediateAvailable = "{{  __('messages.candidate.immediate_available') }}";
        let notImmediateAvailable = "{{  __('messages.candidate.not_immediate_available') }}";
    </script>
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ mix('assets/js/custom/custom-datatable.js') }}"></script>
    <script src="{{ mix('assets/js/candidate/candidate-list.js') }}"></script>
@endpush
