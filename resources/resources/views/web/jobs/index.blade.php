@extends('web.layouts.app')
@section('title')
    {{ __('web.job_menu.search_job') }}
@endsection
@section('css')
    <link href="{{ asset('assets/css/ion.rangeSlider.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('page_css')
@endsection
@section('content')
    <!-- =============== Start of Page Header 1 Section =============== -->

    
    <section class="page-header">
        <div class="container">
            <!-- Start of Page Title -->
            <div class="row">       
            <hr>
            <div class="col-md-12">
        
            <form  class="dezPlaceAni" action="{{ route('front.search.jobs') }}" method="get">
  <div class="form-row">
    <div class="form-group col-lg-5">
    <div class="job-searchs">
    <input type="text" name="keywords" id="search-keywords"
                                   placeholder="Job title, skill or company"
                                   autocomplete="off" class="form-control" style="
    white-space: nowrap;
    display: inline-block;
    padding: 0.75rem 1rem;
    border: 0.0625rem solid rgba(0,0,0,0.16);
    border-radius: 0.5rem;
    background-color: #fff;
    color: #2d2d2d;
    height: 41px;
    font-size: 0.875rem;
    letter-spacing: 0;
    font-weight: 700;
    line-height: 1.58;
    font-family: "Helvetica Neue",Helvetica,Arial,"Liberation Sans",Roboto,Noto,sans-serif;">
                            <div id="jobsSearchResults" class="position-absolute w100"></div>



     
      </div>
    </div>
    <div class="form-group col-lg-5">
    <div class="job-searchs">
    <input type="text" name="location" id="search-location" placeholder="Location" class="form-control" style="white-space: nowrap;
    display: inline-block;
    padding: 0.75rem 1rem;
    border: 0.0625rem solid rgba(0,0,0,0.16);
    border-radius: 0.5rem;
    background-color: #fff;
    color: #2d2d2d;
    height: 41px;
    font-size: 0.875rem;
    letter-spacing: 0;
    font-weight: 700;
    line-height: 1.58;
    font-family: "Helvetica Neue",Helvetica,Arial,"Liberation Sans",Roboto,Noto,sans-serif;">
      
      </div>
    </div>
    <div class="form-group col-lg-2">
    <button type="submit" class="btn btn-primary" >Find Jobs</button>
    </div>
  </div>
  
 
 

  
</form>
 
            
                </div>
            
            
                   
            </div>
            <!-- End of Page Title -->
        </div>
    </section>
    
    <div class="content-block">
			<!-- Browse Jobs -->
			<div class="section-full browse-job p-b50">
				<div class="container">
					<div class="row">
						<div class=" col-lg-7">
                        
                        @livewire('job-search')
                      
						
							
						</div>
						
					</div>
				</div>
			</div>
            <!-- Browse Jobs END -->
		</div>
    

@endsection
@section('page_scripts')
@endsection
@section('scripts')
    <script>
        $('.selectpicker').selectpicker({
            dropupAuto: false,
        });
        let input = JSON.parse('@json($input)');
    </script>
    <script src="{{ asset('assets/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ mix('assets/js/jobs/front/job_search.js') }}"></script>
@endsection
