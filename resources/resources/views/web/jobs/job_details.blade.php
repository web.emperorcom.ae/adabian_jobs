@extends('web.layouts.app')
@section('title')
    {{ __('web.job_details.job_details') }}
@endsection
@section('content')

<section class="ptb15 bg-gray" id="job-page">
@if($job->is_suspended || !$isActive)
                <div class="row">
                    <div class="col-md-12 col-sm-12 pl-0">
                        <div class="alert alert-warning text-warning bg-transparent" role="alert">
                            {{ __('web.job_details.job_is') }} <strong> {{\App\Models\Job::STATUS[$job->status]}}
                                .</strong>
                        </div>
                    </div>
                </div>
            @endif
            @if(Session::has('warning'))
                <div class="alert alert-warning" role="alert">
                    {{ Session::get('warning') }}
                    <a href="{{ route('candidate.profile',['section'=> 'resumes']) }}"
                       class="alert-link ml-2 ">{{ __('web.job_details.click_here') }}</a> {{ __('web.job_details.to_upload_resume') }}
                    .
                </div>
            @endif
<div class="container">
        <div class="row">
            <div class="col-md-8">

                

                

                <div class="job-view-container box-shadow bg-white p-4 mb-4">

                    <h4 >{{ html_entity_decode(Str::limit($job->job_title,50,'...')) }}</h4>

                    <p class="text-muted">
                        <i class="la la-briefcase"></i> Full Time
                        <i class="la la-map-marker"></i>
                        @if (!empty($job->city_id))
                                        {{$job->city_name}} ,
                                    @endif

                                  

                                    @if (!empty($job->country_id))
                                        {{$job->country_name}}
                                    @endif

                                    @if(empty($job->country_id))
                                        Location Information not available.
                                    @endif
                        
                        <i class="la la-clock-o"></i> Posted On: {{ date('jS M, Y', strtotime($job->job_expiry_date)) }}
                    </p>

                                            <div class="job-view-single-section my-4">
                            <h5 class="mb-4">Skills</h5>
                            
                            @if($job->jobsSkill->isNotEmpty())
                                    <span>{{html_entity_decode($job->jobsSkill->pluck('name')->implode(', ')) }}</span>
                                @else
                                    <p>N/A</p>
                                @endif
                                                                                    </div>
                    
                                            <div class="job-view-single-section my-4">
                            <h5 class="mb-4">Description</h5>
                            <p>
                            @if($job->description)
                                        <p>{!! nl2br($job->description) !!} </p>
                                    @else
                                        <p>N/A</p>
                                    @endif
                            </p>
                        </div>
                    
                                            
                    
                                            <div class="job-view-single-section my-4">
                            <h5 class="mb-4">Educational Requirements</h5>
                            <ul><li class="mb-2">Minimum Bachelor Degree
</li><li class="mb-2">Certification of MS Office</li></ul>
                        </div>
                    
                                            <div class="job-view-single-section my-4">
                            <h5 class="mb-4">Experience Requirements</h5>
                        {{ isset($job->experience) ? $job->experience .' '. __('messages.candidate_profile.year') :'No experience' }}
                        </div>

                        <div class="job-view-single-section my-4">
                            <h5 class="mb-4">Remote Work</h5>
                            {{ $job->is_freelance == 1 ? __('messages.common.yes') : __('messages.common.no') }}
                        </div>
                    
                        <div class="job-view-single-section my-4">
                            <h5 class="mb-4">COVID-19 Precaution(s):</h5>
                            <ul><li class="mb-2">Personal protective equipment provided or required
</li><li class="mb-2">Social distancing guidelines in place</li><li class="mb-2">Virtual meetings</li><li class="mb-2">Sanitizing, disinfecting, or cleaning procedures in place</li></ul>
                        </div>
                                      
                                            
                        

                    
                    


                    <div class="terms-msg mt-5 mb-3">
                        <p class="text-small text-muted font-italic">By applying to a job using Adabian you are agreeing to comply with and be subject to the Adabian  <a href="">Terms and Conditions</a> for use of our website. To use our website, you must agree with the <a href="">Terms and Conditions</a> and both meet and comply with their provisions.
                        </p>
                    </div>


                </div>

            </div>

            <div class="col-md-4">

                
                <div class="widget-box bg-white p-3 mb-3 box-shadow">
                    <h5>Job Summary</h5>

                                            <p> <i class="la la-user"></i>No of Roles: {{ isset($job->position)?$job->position:'0' }} </p>
                                            <p> <i class="la la-user"></i>Location : @if (!empty($job->city_id))
                                        {{$job->city_name}} ,
                                    @endif

                                    @if (!empty($job->state_id))
                                        {{$job->state_name}},
                                    @endif

                                    @if (!empty($job->country_id))
                                        {{$job->country_name}}
                                    @endif

                                    @if(empty($job->country_id))
                                        Location Information not available.
                                    @endif</p>
                                            <p> <i class="la la-user"></i>Expires On: {{ date('jS M, Y', strtotime($job->job_expiry_date)) }}</p>
                                            <p> <i class="la la-user"></i>Job Skills :  @if($job->jobsSkill->isNotEmpty())
                                    <span>{{html_entity_decode($job->jobsSkill->pluck('name')->implode(', ')) }}</span>
                                @else
                                    <p>N/A</p>
                                @endif</p>
                   

                    <p>
                    @auth
                            @role('Candidate')
                            @if(!$isApplied && !$isJobApplicationRejected && ! $isJobApplicationCompleted)
                                <div class="mt20">
                                    @if($isActive && !$job->is_suspended && \Carbon\Carbon::today()->toDateString() < $job->job_expiry_date->toDateString())
                                        <button
                                            class="btn {{ $isJobDrafted ? 'btn-red' : 'btn-purple' }} btn btn-primary"
                                            onclick="window.location='{{ route('show.apply-job-form', $job->job_id) }}'">
                                            {{ $isJobDrafted ? __('web.job_details.edit_draft') : __('web.job_details.apply_for_job') }}
                                        </button>
                                    @endif
                                </div>
                            @else
                                <div class="mt20">
                                    <p>
                                        <button class="btn btn-primary">{{ __('web.job_details.already_applied') }}</button>
                                    </p>
                                </div>
                            @endif
                            @endrole
                        @else
                            @if($isActive && !$job->is_suspended && \Carbon\Carbon::today()->toDateString() < $job->job_expiry_date->toDateString())
                                <div class="mt20">
                                  
                                    <button class="btn btn-primary"
                                            onclick="window.location='{{ route('front.candidate.login') }}'">
                                        {{ __('web.job_details.apply_for_job') }}
                                    </button>
                                </div>
                            @endif
                        @endauth
                       
                    </p>
                    <h5>Share Job</h5>
                    <a href="#" class="btn btn-primary share s_facebook"><i class="la la-facebook"></i> </a>
                        <a href="#" class="btn btn-danger share s_plus"><i class="la la-google-plus"></i> </a>
                        <a href="#" class="btn btn-info share s_twitter"><i class="la la-twitter"></i> </a>
                        <a href="#" class="btn btn-primary share s_linkedin"><i class="la la-linkedin"></i> </a>
                </div>


                <div class="widget-box bg-white p-3 mb-3 box-shadow">

                    <div class="job-view-company-logo mb-3">

                    <a href="#">
                                        <img src="{{ $job->company->company_url }}"
                                        class="img-fluid" style="
    width: 100px;
"/>
                                    </a>
                       
                    </div>

                    <h5>{{ html_entity_decode($job->company->user->first_name) }}</h5>

                    <p class="text-muted">
                        <i class="la la-map-marker"></i>
                        @if (!empty($job->company->city_name))
                                                {{$job->company->city_name}} ,
                                            @endif

                                            @if (!empty($job->company->state_name))
                                                {{$job->company->state_name}},
                                            @endif

                                            @if (!empty($job->company->country_name))
                                                {{$job->company->country_name}}
                                            @endif

                                            @if(empty($job->company->country_name))
                                                {{ __('web.job_details.location_information_not_available') }}
                                            @endif
                                            </p>

                    
                                           
                    
                    
                                           
                    
                    <p>
                    <a href="#"><span
                                                    class="label label-info mt20">{{ $jobsCount }} {{ __('web.companies_menu.opened_jobs') }}</span></a>
                                                                        </p>

                </div>


       

                


            </div>
        </div>
    </div>

    </section>
@endsection
@section('scripts')
    <script>
        let addJobFavouriteUrl = "{{ route('save.favourite.job') }}";
        let reportAbuseUrl = "{{ route('report.job.abuse') }}";
        let emailJobToFriend = "{{ route('email.job') }}";
        let isJobAddedToFavourite = "{{ $isJobAddedToFavourite }}";
        let removeFromFavorite = "{{ __('web.job_details.remove_from_favorite') }}";
        let addToFavorites = "{{ __('web.job_details.add_to_favorite') }}";
    </script>
    <script src="{{ mix('assets/js/jobs/front/job_details.js') }}"></script>
@endsection
