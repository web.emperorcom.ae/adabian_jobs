@extends('web.layouts.app')
@section('title')
    {{ __('web.home') }}
@endsection
@section('page_css')
    
@endsection
@section('content')

    <!-- header END -->
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white">Terms and Conditions</h1>
					<!-- Breadcrumb row -->
					<div class="breadcrumb-row">
						<ul class="list-inline">
							<li><a href="#">Home</a></li>
							<li>Terms and Conditions</li>
						</ul>
					</div>
					<!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
		<div class="content-block">
            <div class="section-full content-inner overlay-white-middle">
				<div class="container">
					<div class="row align-items-center m-b50">
						<div class="col-md-12 col-lg-12 m-b20">
						
						
							<p class="m-b15">This describes the terms and conditions on which Adabian.com allows you to access and use the  web site . By using the Web Site, you (User) accept and agree to be bound the terms  and Conditions. Please read this page carefully. If you do not accept the Terms and Conditions stated here, do not use the Web Site and service. The terms “you” and “User” as used herein refer to all individuals and/or entities accessing the Web Site for any reason.</p>
							<h3 class="fw4">Use of  WebSite Contents</h3>
                            <p class="m-b15"> The Company authorizes you to view and download a single copy of the material on the Web Site solely for your personal, noncommercial use. This license to use the Web Site Content  is not a sale of any of the owner's rights. The Web Site may be used only by you, and you may not rent, lease, lend, sub-license or transfer the Web Site or any data residing on it or any of your rights under this agreement to anyone else. You may not develop or derive for commercial sale any data in machine-readable or other form that incorporates or uses any substantial part of the Web Site. You may not transfer to or store any data residing or exchanged over the Web Site to any electronic network for use by more than one user unless you obtain prior written permission from the Company.
You must retain all copyright, trademark, service-mark and other proprietary notices contained in the original Material on any copy you make of the Material. Except as otherwise expressly provided, you may not sell, modify, reproduce, copy, display, perform, distribute, transfer, use, publish, license or create derivate works from any Material or content contained on the Web Site. 
The use of the Material on any other web site or in a networked computer environment for any purpose is prohibited. You shall not copy or adapt the HTML code that the Company creates to generate its pages. It is also protected by the United Arab Emirates and foreign copyright laws.
The Company may revise these Terms and Conditions at any time by updating this posting. You should visit this page periodically to review the Terms and Conditions, because they are binding on you.
 </p>
 <h3 class="fw4">  License</h3>
 <p class="m-b15" style="
    text-align: justify;
">•	- License to Use by Job Seekers.  – Adabian.com hereby grants you a limited, terminable, non-exclusive right to access and use the Site only for your personal use seeking employment opportunities for yourself. This authorizes you to view and download a single copy of the material on the Site solely for your personal, non-commercial use. Your use of the Site is a privilege. Adabian.com reserves the right to suspend or terminate that privilege for any reason at any time, in its sole discretion.
<br>•	- License to Use by Employers. Adabian.com hereby grants you a limited, terminable, non-exclusive right to access and use the Site only for your internal business use seeking candidates for employment. This authorizes you to view and download a single copy of the material on the Site solely for your personal use directly related to using the Site for the purpose of searching and recruiting job prospects. Adabian.com reserves the right to suspend or terminate your access and use at any time if Adabian.com determines that you are in breach of any of these Terms and Conditions.
You represent, warrant and agree that you will not use (or plan, encourage or help others to use) the Site for any purpose or in any manner that is prohibited by these Terms and Conditions or by applicable law. It is your responsibility to ensure that your use of the Site complies with these Terms and Conditions.
 </p>
 <h3 class="fw4"> Security Rules</h3>
 <p class="m-b15" style="
    text-align: justify;
">Users are prohibited from violating or attempting to violate the security of the Web Site, including, without limitation:
 (a) accessing data not intended for such user or logging into a server or account which the user is not authorized to access 
(b) attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorization
 (c) attempting to interfere with service to any user, host or network, including, without limitation, via means of submitting a virus to the Web Site, overloading, flooding, spamming, mailbombing or crashing
(d) sending unsolicited e-mail, including promotions and/or advertising of products or services
(e) forging any TCP/IP packet header or any part of the header information in any e-mail or newsgroup posting.
Violations of system or network security may result in civil or criminal liability. The Company will investigate occurrences which may involve such violations and may involve, and cooperate with, law enforcement authorities in prosecuting users who are involved in such violations.
 </p>
 <h3 class="fw4"> User Data</h3>
 <p class="m-b15" style="
    text-align: justify;
">You will be required as part of your registration for the Web Site to provide the Company with certain information about yourself, including, a valid e-mail address (your Information). In addition to the terms of any privacy policy posted on the Web Site, you understand and agree that the Company may disclose to third parties, on an anonymous basis, certain aggregate information contained in your registration application. However, the Company will not disclose to any third party your name, address, e-mail address or telephone number without your prior consent, except to the extent required by applicable laws or in legal proceedings. The Company reserves the right to offer third party services and products to you based on the preferences that you identify in your registration and at any time thereafter; such offers may be made by the Company or by third parties. </p>
 <h3 class="fw4" >  Supervision</h3>
 <p class="m-b15" style="
    text-align: justify;
">The Company acts as a passive conduct for the distribution and publication of user-submitted material and has no obligation to screen or monitor said material. If notified by a user of material which allegedly do not conform to these Terms and Conditions, the Company may investigate the allegation and determine in good faith and its sole discretion whether to remove or request the removal of the communication. The Company has no liability or responsibility to users for performance or nonperformance of such activities. The Company reserves the right to (a) expel users and prevent their further access to the Web Site for violating these Terms and Conditions or the law, (b) remove communications which are abusive, illegal, or disruptive, or (c) take any action with respect to user-submitted material that it deems necessary or appropriate in its sole discretion if it believes it may create liability for the Company or may cause the Company to lose (in whole or in part) the services of its ISPs or other suppliers. </p>
 <h3 class="fw4"> Third Party policy</h3>
 <p class="m-b15" style="
    text-align: justify;
">The Web Site contains links or other connections to third party web sites. The Company provides these links only as a convenience to you and does not endorse, and is not responsible for, the contents on such linked sites. Further, The Company is not responsible for any viruses accesses through said third-party. If you decide to access linked third party Web sites, you do so at your own risk. </p>
 <h3 class="fw4"> Registration and Login Credentials </h3>
 <p class="m-b15" style="
    text-align: justify;
">You are responsible for maintaining the confidentiality of your information and login credentials. You shall be responsible for all uses and activities that occur under your password, regardless of whether they are authorized by you. You agree to immediately notify the Company of any unauthorized use of your registration or login credentials. The Company will not be liable for any loss or damage arising from an unauthorized use of your registration or password. </p>
 <h3 class="fw4"> 	The Company's Liability</h3>
 <p class="m-b15" style="
    text-align: justify;
"> The Web Site acts as a venue for employers to post job opportunities and candidates to post resumes and does not screen or censor the listings offered. The Company is not involved in the actual transaction between employers and candidates. Therefore, the Company has no control over the quality, safety or legality of the jobs, resumes or other material posted, the truth or accuracy of the listings, the ability of employers to offer job opportunities to candidates or the ability of candidates to fill job openings.
 You acknowledge and agree that you are solely responsible for the form, content and accuracy of any resume or material placed by you on the Web Site. Employers are solely responsible for their postings on the Web Site. The Company is not responsible for any material posed on the Web Site that may be offensive, harmful or inaccurate. Further, the Company cannot and does not confirm that each user is who they claim to be.
The Company is not to be considered to be an employer with respect to your use of the Web Site and the Company shall not be responsible for any employment decisions made by any entity posting jobs on the Web Site. We make no warranties whatsoever that you will obtain any job via the Web Site, nor any warranties about (and take no responsibility for) any job you may obtain. In the event that you have a dispute with another user, you release the Company, its agents and employees from claims, demands and damages (actual and consequential, direct and indirect) of every kind and nature, known and unknown, arising out of or in any way connected with such disputes.
The Material may contain inaccuracies or typographical errors. The Company makes no representations about the truthfulness, accuracy, reliability, completeness, or timeliness of the Web Site or the Material or material posted by users. The use of the Web Site and the Material and any reliance on material posted by other users is at your own risk. Changes are periodically made to the Web Site and may be made at any time.
<br>THE COMPANY DOES NOT WARRANT THAT THE WEB SITE WILL OPERATE ERROR-FREE OR THAT THE WEB SITE AND ITS SERVER (OR ANY SOFTWARES AND MATERIALS ACCESSIBLE THROUGH THE WEB SITE) ARE FREE OF COMPUTER VIRUSES OR OTHER HARMFUL MECHANISMS. IF YOUR USE OF THE WEB SITE OR THE MATERIAL RESULTS IN THE NEED FOR SERVICING OR REPLACING EQUIPMENT OR DATA, THE COMPANY IS NOT RESPONSIBLE FOR THOSE COSTS.
THE WEB SITE AND MATERIAL ARE PROVIDED ON AN AS IS AND AS AVAILABLE BASIS WITHOUT ANY WARRANTIES OF ANY KIND. THE COMPANY, TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, THE WARRANTY OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. THE COMPANY MAKES NO WARRANTIES ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS OF THE MATERIAL, SERVICES, SOFTWARE, TEXT, GRAPHICS, AND LINKS.
IN NO EVENT SHALL THE COMPANY, ITS SUPPLIERS, OR ANY THIRD PARTIES MENTIONED ON THE WEB SITE BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DIRECT, INDIRECT, PUNITIVE, SPECIAL, INCIDENTAL AND CONSEQUENTIAL DAMAGES, LOST PROFITS, OR DAMAGES RESULTING FROM LOST DATA OR BUSINESS INTERRUPTION) ARISING OUT OF OR IN ANY WAY CONNECTED TO THE USE OR INABILITY TO USE THE WEB SITE (OR ANY LINKED SITES) AND THE MATERIAL OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS AND SERVICES THROUGH THE WEB SITE, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT THE COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY. IF THIS LIMITATION OR LIABILITY OR THE EXCLUSION OF WARRANTY SET FORTH ABOVE IS HELD INAPPLICABLE OR UNENFORCEABLE FOR ANY REASON, THEN THE COMPANY'S MAXIMUM LIABILITY FOR ANY TYPE OF DAMAGES SHALL BE LIMITED TO U.S. $100.00 PER OCCURENCE.
In case of any disputes, the law applicable would be based on the UAE Governing Law.
</p>
 <h3 class="fw4"> 	Termination and Equitable Relief</h3>
 <p class="m-b15" style="
    text-align: justify;
">In the event of any breach by you of these Terms and Conditions or if the Company is unable to verify or authenticate any information you submit, the Company may, at its sole discretion, pursue all of its legal remedies, including, but not limited to, deletion of your postings from the Web Site and/or immediate termination of your registration or ability to access the Web Site or any other service provided to you by the Company. The Company shall have the right to terminate your ability to access the Web Site at any time without notice and to discontinue or modify any of the information contained on the Web Site, or the Web Site itself, at any time. Given the nature of these Terms and Conditions, you understand and agree that, in addition to money damages, the Company will be entitled to equitable relief upon a breach of them by you. </p>
 <h3 class="fw4"> Indemnity</h3>
 <p class="m-b15" style="
    text-align: justify;
">You agree to defend, indemnify, and hold harmless the Company, its officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from your use of the Material or your breach of the terms of these Terms and Conditions or your representations and warranties or your violation of any rights of third parties, including without limitation, any proprietary or intellectual property rights. </p>
 <h3 class="fw4"> Miscellaneous</h3>
 <p class="m-b15" style="
    text-align: justify;
">1.	Validity of Terms and Conditions: A printed version of these Terms and Conditions and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. You agree that the fact that these Terms and Conditions are in electronic form does not affect in any way their validity or enforceability. You also agree that the clicking of the button I accept on the Web Site constitutes your valid and legal signature.
<br>2.	Other Jurisdictions: Access to the Materials may not be legal by certain persons or in certain countries. When you access the Web, you do so at your own risk and are responsible for compliance with the laws of your jurisdiction. Use of the Web Site is unauthorized in any jurisdiction that does not give effect to all provisions of these Terms and Conditions.
<br>3.	Assignment; Waiver: You may not assign any part of this Agreement without the Company's prior written consent. No waiver of any obligation or right of either party shall be effective unless in writing, executed by the party against whom it is being enforced.
<br>4.	Governing Law; Venue: In case of any disputes, the law applicable would be based on the UAE Governing Law.
<br>5.	Severability: If any provision of these Terms and Conditions are governed by the internal substantive laws of UAE. Jurisdiction for any claims arising under this agreement shall lie exclusively with the state or federal courts located in United Arab Emirates.
<br>6.	Entire Agreement: Except as expressly provided in additional terms of use for areas of the Web, these Terms and Conditions constitute the entire agreement between you and the Company with respect to the use of the Web Site. No changes to these Terms and Conditions shall be made except by a revised posting on this page. Certain areas of the Web Site are subject to additional terms of use. By using such areas, or any part thereof, you agree to be bound by the additional terms of use applicable to such areas.
 </p>
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- Why Chose Us -->
			<!-- Call To Action -->
			
			<!-- Call To Action END -->
			<!-- Our Latest Blog -->
			
			<!-- Our Latest Blog -->
        </div>
		<!-- contact area END -->
    </div>
    <!-- Content END-->
	<!-- Modal Box -->
	
	<!-- Modal Box End -->
	<!-- Footer -->
    @endsection
@section('page_scripts')
    <script>
        var availableLocation = [];
        @foreach(getCountries() as $county)
        availableLocation.push("{{ $county  }}");
        @endforeach
    </script>
    <script src="{{mix('assets/js/home/home.js')}}"></script>
@endsection

