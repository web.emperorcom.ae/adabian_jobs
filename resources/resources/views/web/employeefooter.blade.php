<!-- =============== Start of Footer 1 =============== -->
<footer class="site-footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
					<div class="col-xl-5 col-lg-4 col-md-12 col-sm-12">
                        <div class="widget">
                            <img src="../images/logo-white.png" width="180" class="m-b15" alt=""/>
							<p class="text-capitalize m-b20">Adabian.com is a reliable chaperon which assist to link your professional portrait with the employer and vice-versa. Whether you be a newborn or a veteran  in the job market, we help you to find the appropriate & authentic employers to connect with.</p>
                            <div class="subscribe-form m-b20">
								<form class="dzSubscribe" action="https://job-board.dexignzone.com/xhtml/script/mailchamp.php" method="post">
									<div class="dzSubscribeMsg"></div>
									<div class="input-group">
										<input name="dzEmail" required="required"  class="form-control" placeholder="Your Email Address" type="email">
										<span class="input-group-btn">
											<button name="submit" value="Submit" type="submit" class="site-button radius-xl">Subscribe</button>
										</span> 
									</div>
								</form>
							</div>
							<ul class="list-inline m-a0">
								<li><a href="https://www.facebook.com/adabiancom/" class="site-button white facebook circle "><i class="fa fa-facebook"></i></a></li>
				
								<li><a href="https://www.linkedin.com/company/adabiancom/" class="site-button white linkedin circle "><i class="fa fa-linkedin"></i></a></li>
								<li><a href="https://www.instagram.com/adabiancom/" class="site-button white instagram circle "><i class="fa fa-instagram"></i></a></li>
								<li><a href="https://twitter.com/adabiancom" class="site-button white twitter circle "><i class="fa fa-twitter"></i></a></li>
							</ul>
                        </div>
                    </div>
					<div class="col-xl-5 col-lg-5 col-md-8 col-sm-8 col-12">
                        <div class="widget border-0">
                            <h5 class="m-b30 text-white">Quick Links</h5>
                            <ul class="list-2 list-line">
                            <li><a href="{{ route('web.home.Aboutus') }}">About Us</a></li>
                                <li><a href="{{ route('web.home.Terms') }}">Terms of Use</a></li>
                                <li><a href="{{ route('web.home.privacypolicy') }}">Privacy Policy</a></li>
                               
                                
                                <li><a href="{{ route('front.candidate.login') }}">Upload Cv</a></li>
                                <li><a href="{ route('web.home.employeer') }}">Post Job</a></li>
                               <li><a href="{{ route('front.contact') }}">Contact us</a></li>
                               
                               
                            </ul>
                        </div>
                    </div>
					<div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-12">
                        <div class="widget border-0">
                            <h5 class="m-b30 text-white">Jobs in UAE</h5>
                            <ul class="list-2 w10 list-line">
                                <li><a href="javascript:void(0);">Dubai Jobs</a></li>
                                <li><a href="javascript:void(0);">Abu dhabi Jobs</a></li>
                                <li><a href="javascript:void(0);">Sharjah Jobs</a></li>
                                <li><a href="javascript:void(0);">Ajman Jobs</a></li>
                           
                            </ul>
                        </div>
                    </div>
				</div>
            </div>
        </div>
        <!-- footer bottom part -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
						<span> © Copyright 2021  by 
                        <a href="http://adabian.com/" target="_blank">Adabian  </a> All rights reserved</span> 
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer END -->
    <!-- scroll top button -->
    <button class="scroltop fa fa-arrow-up" ></button>
<!-- =============== End of Footer 1 =============== -->

<!-- ===== Start of Back to Top Button ===== -->

<!-- ===== End of Back to Top Button ===== -->

<!-- ===== Start of Login Pop Up div ===== -->

<!-- cd-user-modal -->
<!-- ===== End of Login Pop Up div ===== -->
