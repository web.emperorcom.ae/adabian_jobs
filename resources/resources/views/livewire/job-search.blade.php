






<div class="col-lg-12 ">
    
    @if(!empty($searchByLocation))
        <div id="jobsSearchResults" class="position-absolute w100">
            <ul class="d-block position-relative w-100">
                @forelse($jobs as $job)
                    <li>{{ $job->job_title }}</li>
                @empty
                  
                @endforelse
            </ul>
        </div>
    @endif
     <div class="job-post-wrapper mt20">
     @forelse($jobs as $job)
     <div class="card">
  <div class="card-header">
  <h5 class="card-title"> <a href="{{ route('front.job.details',$job['job_id']) }}">{{ html_entity_decode($job['job_title']) }} </a></h5>

  
  <h7 class="card-title"> <a href="{{ route('front.job.details',$job['job_id']) }}">{{ $job->company->user->first_name }} </a></h7><br>
  <h7 class="card-title"> <a href="{{ route('front.job.details',$job['job_id']) }}">{{ (!empty($job->full_location)) ? $job->full_location : 'Location Info. not available.'}} </a></h7><br>
  <span>AED{{ $job['salary_from'] }}-AED{{ $job['salary_to'] }}</span>
  </div>
  <div class="card-body">
    
    <h5 class="card-title">Requirements</h5>
 
    <p class="card-text"> {!! nl2br(Str::limit($job['description'],200)) !!}  </p>
    
    <a href="{{ route('front.job.details',$job['job_id']) }}" class="btn btn-primary">Easily Apply</a>
    
    
    <br><br>
    <span title="{{ date('jS M, Y', strtotime($job->created_at)) }}">{{ $job->created_at->diffForHumans() }}</span>
   
  
  </div>

  
</div><br>

@empty

            <div class="col-lg-12">
            <div>
            <div class="card">
  <div class="card-header">
  <img src="images/no-search.png" alt=""/>
            <h3>Whoops, no matches  </h3>
            <h5 class="text-muted">We couldn't find any search results. </h5>
            <h5 class="text-muted">Give it another try</h5>
            
            </div>
            </div>
            </div>
            </div>
            
        @endforelse

        @if($jobs->count() > 0)
            {{$jobs->links() }}
        @endif





    </div>

















</div>