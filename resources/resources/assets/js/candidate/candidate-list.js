'use strict';

let tableName = '#candidatesTbl';
let tbl = $('#candidatesTbl').DataTable({
    processing: true,
    serverSide: true,
    scrollX: false,
    deferRender: true,
    scroller: true,
    'order': [[0, 'asc']],
    ajax: {
        url: candidateUrl,
        data: function (data) {
            data.is_status = $('#filter_status').
                find('option:selected').
                val();
            data.is_immediate_available = $('#immediateAvailable').
                find('option:selected').
                val();
        },
    },
    columnDefs: [
        {
            targets: '_all',
            defaultContent: 'N/A',
        },
        {
            'targets': [4, 5, 6, 7],
            'className': 'text-center',
            'width': '5%',
            'orderable': false,
        },
        {
            'targets': [8],
            'visible': false,
        },
    ],
    columns: [
        {
            data: function (row) {
                let showUrl = candidateUrl + '/' + row.id;
                let element = document.createElement('textarea');
                element.innerHTML = row.user.full_name;
                return '<a href="' + showUrl + '">' + element.value +
                    '</a>';
            },
            name: 'user.first_name',
        },
        {
            data: 'user.email',
            name: 'user.email',
        },
        {
            data: function (row) {
                if (row.industry_id == null) {
                    return 'N/A';
                } else {
                    return row.industry.name;
                }
            },
            name: 'industry.name',
        },
        {
            data: function (row) {
                if (row.immediate_available == 0) {
                    return notImmediateAvailable;
                } else {
                    return immediateAvailable;
                }
            },
            name: 'immediate_available',
        },
        {
            data: function (row) {
                let checked = row.user.email_verified_at != null
                    ? 'checked disabled'
                    : '';
                let data = [{ 'id': row.id, 'checked': checked }];
                return prepareTemplateRender('#isEmailVerified', data);
            },
            name: 'user.email_verified_at',
        },
        {
            data: function (row) {
                let url = candidateUrl + '/' + row.id;
                let data = [
                    {
                        'id': row.id,
                        'url': url + '/send-email-verification',
                    }];
                return prepareTemplateRender('#resendEmail', data);
            },
            name: 'user.email_verified_at',
        },
        {
            data: function (row) {
                let checked = row.user.is_active === 0 ? '' : 'checked';
                let data = [{ 'id': row.id, 'checked': checked }];
                return prepareTemplateRender('#isActive', data);
            },
            name: 'user.is_active',
        },
        {
            data: function (row) {
                let url = candidateUrl + '/' + row.id;
                let data = [
                    {
                        'id': row.id,
                        'url': url + '/edit',
                    }];
                return prepareTemplateRender('#candidateActionTemplate', data);
            }, name: 'id',
        },
        {
            data: 'user.last_name',
            name: 'user.last_name',
        },
    ],
    'fnInitComplete': function () {
        $('#filter_status, #immediateAvailable').change(function () {
            $(tableName).DataTable().ajax.reload(null, true);
        });
    },
});

$(document).ready(function () {
    $('#filter_status').select2();
});
$(document).ready(function () {
    $('#immediateAvailable').select2();
});

$(document).on('click', '.delete-btn', function (event) {
    let candidateId = $(event.currentTarget).data('id');
    console.log(candidateId);
    deleteItem(candidateUrl + '/' + candidateId, tableName, 'Candidate');
});

$(document).on('change', '.isActive', function (event) {
    let candidateId = $(event.currentTarget).data('id');
    $.ajax({
        url: candidateUrl + '/' + candidateId + '/' + 'change-status',
        method: 'post',
        cache: false,
        success: function (result) {
            if (result.success) {
                $(tableName).DataTable().ajax.reload(null, false);
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
    });
});

$(document).on('change', '.is-candidate-email-verified', function (event) {
    if ($(this).is(':checked')) {
        let candidateId = $(event.currentTarget).data('id');
        changeEmailVerified(candidateId);
        $(this).attr('disabled', true);
    } else {
        return false;
    }
});

window.changeEmailVerified = function (id) {
    $.ajax({
        url: candidateUrl + '/' + id + '/verify-email',
        method: 'post',
        cache: false,
        success: function (result) {
            if (result.success) {
                displaySuccessMessage(result.message);
                $(tableName).DataTable().ajax.reload(null, false);
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
    });
};

$(document).on('click', '.send-email-verification', function (event) {
    let candidateId = $(event.currentTarget).attr('data-id');
    $.ajax({
        url: candidateUrl + '/' + candidateId + '/resend-email-verification',
        type: 'post',
        success: function (result) {
            if (result.success) {
                displaySuccessMessage(result.message);
                $(tableName).DataTable().ajax.reload(null, false);
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
    });
});
